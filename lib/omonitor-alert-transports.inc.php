<?php

if (isset($SMTP_HOST)) require_once('Net/SMTP.php');
if (isset($XMPP_CONNECTION)) require_once('XMPPHP/XMPP.php');
if (isset($AWSSMS_REGION)) require_once('aws.phar');

function send_alert_transport_test($recipient,$subject,$body) {
    write_log("test alert for '$recipient': $subject");
}

function send_alert_transport_jira($recipient,$subject,$body) {
    global $JIRA_URL,$JIRA_USER,$JIRA_PASSWORD;
    
    if (!$JIRA_URL) {
        write_log("ERROR \$JIRA_URL not defined, but alert generated for $recipient");
        return;
    }

    if (preg_match("!^(\w+)/(\w+)/(\w+)/(\S+)$!",$recipient,$match)) {
        list($xxx,$projectkey,$issuetype,$priority,$username) = $match;
        $url = $JIRA_URL."/rest/api/2/issue";
        $data = array (
            'fields' => array(
                'project' => array(
                    'key' => $projectkey
                ),
                "issuetype" => array(
                    "name" => $issuetype
                ),
                "assignee" => array(
                    "name" => $username
                ),
                "priority" => array( 
                    "name" => $priority
                ),
                'summary' => $subject,
                'description' => $body,
            ),
        );

        $ch = curl_init();
        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json'
        );

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, "$JIRA_USER:$JIRA_PASSWORD");

        $result = curl_exec($ch);
        $ch_error = curl_error($ch);
        $info = curl_getinfo($ch);

        if ($ch_error) {
            write_log("ERROR: cURL error going to $url: $ch_error");
        } else {
            $http_code = $info["http_code"];
            if ($http_code == 201) {
                $result = json_decode($result);
                $issue = $result->key;
                write_log("JIRA issue created: $JIRA_URL/browse/$issue");
            } elseif ($http_code == 400) {
                write_log("ERROR creating JIRA issue: $result");
            } else {
                write_log("UNKNOWN creating JIRA isssue: $result");
            }
        }
        curl_close($ch);
    } else {
        write_log("ERROR. Recipient $recipient could not be parsed by jira transport handler");
    }
}


function send_alert_transport_email($recipient,$subject,$body) {
    send_mail(array($recipient),$subject,$body);
}

function send_alert_transport_smsemail($recipient,$subject,$body) {
    $body = preg_replace("!^.*?https?://.*?$!ms","",$body);
    $body = preg_replace("!^.*{green}.*?$!ms","",$body);
    send_mail(array($recipient),$subject,$body);
}

function send_alert_transport_xmpp($recipient,$subject,$body) {
    global $XMPP_CONNECTION;
    //$XMPP_CONNECTION="yyyyyyyyyyyyyy@globallogic.com:xxxxxxxxxxxxx/localhost:5222/omonitor";
    if (preg_match("!^(.+)@(.+):(.+)/(.+):(\d+)/(.+)$!",$XMPP_CONNECTION,$match))
        list($xxx,$XMPP_USER,$XMPP_DOMAIN,$XMPP_PASSWORD,$XMPP_HOST,$XMPP_PORT,$XMPP_RESOURCE) = $match;
    if ( $XMPP_USER != "" ) {
        write_debug("connecting to $XMPP_HOST, $XMPP_PORT, $XMPP_USER, $XMPP_RESOURCE, $XMPP_DOMAIN, sending to $recipient");
        $conn = new XMPPHP_XMPP($XMPP_HOST, $XMPP_PORT, $XMPP_USER, $XMPP_PASSWORD, $XMPP_RESOURCE,
            $XMPP_DOMAIN, $printlog=false, $loglevel=XMPPHP_Log::LEVEL_INFO);
        try {
            $conn->connect();
            $conn->processUntil('session_start');
            $conn->presence();
            $conn->message($recipient, "$subject\n$body");
            $conn->disconnect();
        } catch(XMPPHP_Exception $e) {
             write_log($e->getMessage());
        }
    }
    else
        write_log("no XMPP connection defined, no message to $recipient");
}

function send_alert_transport_awssms($recipient,$subject,$body) {
    global $AWSSMS_PROFILE, $AWSSMS_REGION, $AWSSMS_SENDER;
    $body = preg_replace("!^.*?https?://.*?$!ms","",$body);
    $body = preg_replace("!^.*{green}.*?$!ms","",$body);
    $client = Aws\Sns\SnsClient::factory(array(
        'version' => '2010-03-31',
        'profile' => $AWSSMS_PROFILE,
        'region'  => $AWSSMS_REGION
    ));
    $payload = array(
        'Message' => "$subject\n$body",
        'PhoneNumber' => $recipient,
        'MessageAttributes' => array( 'AWS.SNS.SMS.SenderID' => array('StringValue' => $AWSSMS_SENDER, 'DataType' => 'String'))
    );
    $result = $client->publish($payload);
}

// $emails must be array!
function send_mail($emails,$subject,$body) {
    global $SMTP_HOST,$SMTP_PORT,$SMTP_USER,$SMTP_PASSWORD,$SMTP_AUTH,$SMTP_TLS,$SMTP_FROM;
    $headers = "X-Mailer: Omonitor\r\n";
    if ($SMTP_FROM)
        $headers .= "From: $SMTP_FROM\r\n";
    $headers .= "Subject: $subject";
    if ($SMTP_HOST) {
        if (! ($smtp = new Net_SMTP($SMTP_HOST,$SMTP_PORT)))
            return "Unable to instantiate Net_SMTP object";
        //$smtp->setDebug(true);
        if (PEAR::isError($res = $smtp->connect())) 
            return $res->getMessage();
        $smtp->auth($SMTP_USER,$SMTP_PASSWORD,$SMTP_AUTH,$SMTP_TLS);
        if (PEAR::isError($res = $smtp->mailFrom($SMTP_FROM)))
            return "Unable to set sender to <$SMTP_FROM>:" . $res->getMessage();
        foreach ($emails as $to) {
            if (PEAR::isError($res = $smtp->rcptTo($to)))
                 return "Unable to add recipient <$to>: " . $res->getMessage();
        }
        if (PEAR::isError($res = $smtp->data($body,$headers)))
            return "Unable to send data: " . $res->getMessage();
        $smtp->disconnect();
        return true;
    } else {
        return mail($email,$subject,$body,$headers,"-f $MAILFROM"); // returns boolean
    }
}
