<?php

// define the default settings
$DEBUG=false;
$PURPLETIME = 20;
$EVENTSPERPAGE = 20;
$EVENTSHISTORYLIMIT = 50;
$EVENTSHISTORYPAGELIMIT = 50;
$DEFAULTPORT=1975;
$SCRIPT = basename($argv[0] ?? null);
$PHPSELF = basename($_SERVER["PHP_SELF"]);
$START_TIME = microtime_float();
$SERVERLOG="logs/server.log";
$PDO_DRIVER_OPTIONS=array();
$AWSSMS_PROFILE='default';
$AWSSMS_SENDER='OMONITOR';
$CERTIFICATE_EXPIRATION_YELLOW_DAYS=14;
$CERTIFICATE_EXPIRATION_RED_DAYS=7;
foreach(array("/usr/bin/fping","/usr/local/bin/fping","/usr/sbin/fping","/usr/local/sbin/fping") as $key)
    if (file_exists($key))
        $FPING = $key;

$ICON_URLS = array(
    "red" => 'icons/red.gif',
    "yellow" => 'icons/yellow.gif',
    "green" => 'icons/green.gif',
    "purple" => 'icons/purple.gif',
    "clear" => 'icons/clear.gif',
    "blue" => 'icons/blue.gif',
);
$ICON_SIZE = 18;

// now user config overrides default settings
if (file_exists("etc/server.inc.php"))
    require_once("etc/server.inc.php");

if (isset($FORWARDERS))
    $FORWARDERS=preg_split("/[\s,]+/",trim($FORWARDERS));
$FORWARDDATA=array();

