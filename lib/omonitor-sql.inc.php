<?php

function sqlsq($text) {
    global $SQL;
    return $SQL->quote($text);
}

function str_replace_first($from, $to, $subject) {
    return preg_replace('/'.preg_quote($from, '/').'/', $to, $subject, 1);
}

// it expects more then one parameter!
function make_query($query) {
    $i = 1;
    while(preg_match("/%%([is])%%/",$query,$match)) {
        $var = func_get_arg($i);
        if ($match[1] == 's')
            $var = sqlsq($var);
        $query = str_replace_first($match[0],$var,$query);
        $i++;
    }
    return $query;
}

function die_sql() {
    global $SQL;
    $ei = $SQL->errorInfo();
    $errmess = $ei[2];
    $errmess = "Error! $errmess";
    write_log($errmess);
    throw new Exception($errmess);
}

// $create has meaning for sqlite only
function init_database($create=false) {
    global $SQL;
    global $DATABASE;
    global $PDO_DSN,$PDO_USER,$PDO_PASSWORD,$PDO_DRIVER_OPTIONS;
    write_debug("init_database: $create");

    if (preg_match("/^sqlite.?:/i",$PDO_DSN)) {
        if (!$create && !file_exists($DATABASE))
            write_log("Error! Database $DATABASE does not exist!");
    }

    try {
        $SQL = new PDO($PDO_DSN,$PDO_USER,$PDO_PASSWORD,$PDO_DRIVER_OPTIONS);
    }
    catch (PDOException $e) {
        write_log('Connection failed: ' . $e->getMessage());
        die();
    }

    if (preg_match("/^mysql:/i",$PDO_DSN)) {
        $SQL->exec("SET NAMES UTF8");
        $SQL->exec("SET OPTION storage_engine=innodb");
    }
}

function close_database() {
    global $SQL;
    unset ($SQL);
    write_debug("close_database");
}
