<?php
/*
    The main library and initialization procedure for omonitor.
*/

$CWD=getcwd();
ini_set("include_path",ini_get("include_path").":$CWD/lib:$CWD/lib/phpsocketdaemon-1.0");

require_once('omonitor-helpers.inc.php');
require_once('omonitor-sql.inc.php');
require_once('omonitor-config.inc.php');
// depends on config
require_once('omonitor-alert-transports.inc.php');
require_once('omonitor-core.inc.php');
