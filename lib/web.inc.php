<?php

/*
    This is a library that does all generation of html
    for user web interface.
*/

		function ntlm_get_msg_str($msg, $start, $unicode = true) {
			$len = (ord($msg[$start+1]) * 256) + ord($msg[$start]);
			$off = (ord($msg[$start+5]) * 256) + ord($msg[$start+4]);
			if ($unicode)
				return str_replace("\0", '', substr($msg, $off, $len));
			else
				return substr($msg, $off, $len);
		}


if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
    if (preg_match("/^Basic (\w+)/i",$_SERVER['HTTP_AUTHORIZATION'],$match))
    {
        list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) = explode(':' , base64_decode($match[1]));
        $_SERVER['REMOTE_USER'] = $_SERVER['PHP_AUTH_USER'];
    }
    if (preg_match("/^NTLM (\w+)/i",$_SERVER['HTTP_AUTHORIZATION'],$match))
    {
        $ntlm_str = base64_decode($match[1]);
        $login = ntlm_get_msg_str($ntlm_str, 36);
        //$domain = ntlm_get_msg_str($ntlm_str, 28);
        //$host = ntlm_get_msg_str($ntlm_str, 44);
        $_SERVER['PHP_AUTH_USER'] = $login ;
        $_SERVER['REMOTE_USER'] = $_SERVER['PHP_AUTH_USER'];
    }
}


function get_page_data($page)
{
    $pages = get_pages();
    $subpages = array();
    foreach($pages as $p=>$pagedata1)
    {
        if ($p == "$page")
            $pagedata = $pagedata1;
        if (preg_match("!^${page}/(\w+)$!",$p,$match))
        {
            $sp = $match[1];
            $subpages[] = $sp;
        }
    }

    if (preg_match("!^/\w+$!",$page,$match))
        $parentpage = "";
    elseif (preg_match("!^(/.+)/\w+$!",$page,$match))
        $parentpage = $match[1];
    else
        $parentpage = null;
    return array($pagedata,$subpages,$parentpage);
}

function get_pages()
{
    if (!file_exists("etc/pages"))
        $data = "/.*/\n";
    else
    {
        $data = file_get_contents("etc/pages");
        $data = preg_replace("/^\s*/m","",$data);
        $data = preg_replace("/#.*$/m","",$data);
        $data = preg_replace("/\n{2,}/m","\n",$data);
    }
    //_dump($data);

    $data = explode("\n",$data);

    $pages = array();
    $page = "";
    $group = "";
    foreach($data as $line)
    {
        $line = trim($line);
        if (preg_match("/^page\s+(\S+)/",$line,$match))
        {
            list($xxx,$page) = $match;
            $pages[$page]["title"] = "";
            if (preg_match("/title=[\"'](.*)[\"']/",$line,$match))
                $pages[$page]["title"] = $match[1];
            if (preg_match("/\btests=(\S+)/",$line,$match))
                $pages[$page]["tests"] = explode(",",$match[1]);
            if (preg_match("/\bnotests=(\S+)/",$line,$match))
                $pages[$page]["notests"] = explode(",",$match[1]);
            $group = "";
            continue;
        }
        if (preg_match("/^group\s+(.*)$/",$line,$match))
        {
            list($xxx,$group) = $match;
            $pages[$page]["groups"][$group]["hosts"] = array();
            continue;
        }
        if (preg_match("/^group-(only|include)\s+(\S+)\s+(.*)$/",$line,$match))
        {
            list($xxx,$xxx,$tests,$group) = $match;
            $pages[$page]["groups"][$group]["hosts"] = array();
            $pages[$page]["groups"][$group]["tests"] = explode(",",$tests);;
            continue;
        }
        if (preg_match("/^group-exclude\s+(\S+)\s+(.*)$/",$line,$match))
        {
            list($xxx,$tests,$group) = $match;
            $pages[$page]["groups"][$group]["hosts"] = array();
            $pages[$page]["groups"][$group]["notests"] = explode(",",$tests);;
            continue;
        }
        if (preg_match("/^(\w[\w.-]+\w)$/",$line,$match))
        {
            list($xxx,$host) = $match;
            $pages[$page]["groups"][$group]["hosts"][] = $host;
            continue;
        }
        if (preg_match("/^([\/!].*[\/!])$/",$line,$match))
        {
            list($xxx,$host) = $match;
            $pages[$page]["groups"][$group]["hosts"][] = $host;
            continue;
        }
    }
    //_dump($pages);
    return $pages;
}

function get_hostnames()
{
    global $SQL;
    $hosts = array();
    foreach($SQL->query("SELECT DISTINCT hostname
        FROM current
        ORDER BY hostname") as $row)
        $hosts[] = $row[0];
    return $hosts;
}

function get_hosts_tests()
{
    global $SQL;
    $tests = array();
    $hosts_tests = array();

    $hosts = get_hostnames();

    foreach($hosts as $host)
    {

        foreach ($SQL->query("SELECT testname, status
            FROM current
            WHERE hostname=".sqlsq($host)."
            ORDER BY testname") as $row)
        {
            list($test,$c) = $row;
            $hosts_tests[$host][$test] = $c;
            $tests[$test] = true;
        }
    }
    $tests = array_keys($tests);
    sort($tests);

    $nghosts = array();
    $ngtests = array();
    $ngstatus = explode(",","red,yellow,purple");
    foreach($hosts as $host)
    {
        foreach($tests as $test)
        {
            $c = $hosts_tests[$host][$test] ?? null;
            if (in_array($c,$ngstatus))
                $ngtests[$test] = $nghosts[$host] = true;
        }
    }
    $nghosts = array_keys($nghosts);
    $ngtests = array_keys($ngtests);

    $res = array($hosts,$tests,$hosts_tests,$nghosts,$ngtests);
    return $res;
}

function get_all_events($hosts=null,$tests=null,$notests=null,$page=0)
{
    global $SQL;
    global $EVENTSHISTORYLIMIT;
    $host_in = "true";
    $test_in = "true";
    $no_test_in = "true";
    if (is_array($hosts)) $host_in = "hostname IN ('".join("','",$hosts)."')";
    if (is_array($tests)) $test_in = "testname IN ('".join("','",$tests)."')";
    if (is_array($notests)) $no_test_in = "testname NOT IN ('".join("','",$notests)."')";
    $where = "WHERE $host_in AND $test_in AND $no_test_in";
    list($nrecords) = $SQL->query("SELECT COUNT(*) FROM history $where")->fetch();
    //echo "$nrecords"; 
    $offset = $page * $EVENTSHISTORYLIMIT;
    $query = "SELECT hostname,testname,timestamp,status,timestamp_prev,status_prev
        FROM history $where
        ORDER BY timestamp DESC LIMIT $EVENTSHISTORYLIMIT OFFSET $offset";
    $res_events = array();
    foreach($SQL->query($query) as $row)
    {
        list($host,$test,$ts,$c,$tsp,$cp) = $row;
        if ($tsp == 0)
            $cp = "clear";
        $res_events[] = array($ts,$host,$test,$cp,$c,$tsp);
    }
    return array($res_events,$nrecords);
}

/*
function process_events_history($events2,$hosts_tests,$hosts_tests_ts)
{
    $events = array();
}
*/
function make_page_all_history($mode,$page=null,$spage)
{

    global $PHPSELF;

    list($xxx,$color) = make_page_all($mode);
    $html = "";

    $html .= make_top_links("all",$mode);

    // show non-green statuses
    list($hosts,$tests,$hosts_tests,$nghosts,$ngtests) = get_hosts_tests();
    $html .= make_table($nghosts,$ngtests,$hosts_tests,$color);

    list($events,$nrecords) = get_all_events(null,null,null,$spage);

    $html .= make_any_history($events,$mode,"all",$spage,$nrecords);

    return array($html,$color);
}


function make_any_history($events,$mode,$page,$spage,$nrecords)
{
    global $EVENTSHISTORYLIMIT, $EVENTSHISTORYPAGELIMIT;
    global $PHPSELF;
    $navhtml = "<table border=1><tr>\n";
    $np = ceil($nrecords / $EVENTSHISTORYLIMIT);
    for ($p=0;$p<$np && $p<$EVENTSHISTORYPAGELIMIT;$p++) {
        $navhtml .= "<td>";
        if ($p != $spage)
            $navhtml .= "<a href='$PHPSELF?m=$mode&p=$page&pp=$p'>";
        $navhtml .= "$p";
        if ($p != $spage)
            $navhtml .= "</a>";
        $navhtml .= "</td>\n";
    }
    $navhtml .= "</tr></table>\n";

    $html = "<table border=1>\n<tr><td>Date</td><td>Host/Test</td><td>Change</td></tr>\n";
    foreach($events as $arr)
    {
        list($ts,$host,$test,$c1,$c2,$ts2) = $arr;
        $c22 = $c2; // for background
        if ($c22 == "clear")  $c22 = "grey";

        preg_match("/^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/",$ts,$match);
        list($xxx,$year,$month,$day,$hour,$min,$sec) = $match;
        $evdate = date("y/m/d H:i:s",gmmktime($hour,$min,$sec,$month,$day,$year));
        $html .= "<tr>\n";
        $html .= "<td>".html($evdate)."</td>\n";
        $html .= "<td style='background: $c22; color: black;'>\n<a style='color: black;' href='hosthistory.php?h=".urlencode($host)."'>".html($host)."</a>\n";
        $html .= " / <a style='color: black;' href='testhistory.php?h=".urlencode($host)."&t=$test'>".html($test)."</a></td>\n<td>";
        $html .= ($ts2)
            ? "<a href='test.php?h=".urlencode($host)."&t=$test&e=$ts2'>".html_icon($c1)."</a>"
            : html_icon($c1);
        $html .= "\n<img src='icons/arrow.gif'>\n";
        $html .= "<a href='test.php?h=".urlencode($host)."&t=$test&e=$ts'>".html_icon($c2)."</a></td>\n";
        $html .= "</tr>\n";
    }

    $html .= "</table>\n";
    return "<p>$navhtml</p><p>$html</p><p>$navhtml</p>";
}

function make_page_all($mode)
{
    global $PHPSELF;
    $html = "";
    $html .= make_top_links("all",$mode);
    list($hosts,$tests,$hosts_tests) = get_hosts_tests();
    $html .= make_table($hosts,$tests,$hosts_tests,$color);
    return array($html,$color);
}


function make_page_groups($mode,$page)
{
    global $PHPSELF;

    $color = "green";

    list($pagedata, $subpages,$parentpage) = get_page_data($page);

    $html = "";
    $html .= make_top_links($page,$mode);

    list($hosts,$tests,$hosts_tests) = get_hosts_tests();
    //_dump($hosts);
    //_dump($tests);
    //_dump($hosts_tests);

    $html .= make_subpages_links($page,$mode,$subpages,$parentpage);

    $pagehosts = array();
    if (is_array($pagedata["groups"]))
    foreach($pagedata["groups"] as $gp=>$pagearr)
    {
        //_dump($pagearr);
        // collect all hosts that match this host-group
        $grouphosts = array();
        foreach ($hosts as $host)
        {
            $skip = 1;
            foreach($pagearr["hosts"] as $hostre)
            {
                if ($hostre == $host)
                    $skip = 0;
                elseif (preg_match("/^[\/!].*[\/!]\w*$/",$hostre) && preg_match("$hostre",$host))
                    $skip = 0;
            }
            if ($skip)
                continue;

            //if ($printedhosts[$host]) continue;

            $pagehosts[$host] = true;
            $grouphosts[$host] = true;

        }
        $pagehosts = array_keys($pagehosts);
        $grouphosts = array_keys($grouphosts);

        $html .= "<p><b>".html($gp)."</b>";

        if (count($grouphosts) == 0)
        {
            $html .= "<br>No hosts found for the group\n";
            continue;
        }

        //sort($grouphosts);

        // collect all tests that have all these hosts
        $grouptests = array();
        foreach($grouphosts as $host)
        {
            if (!is_array($hosts_tests[$host]))
                continue;
            foreach(array_keys($hosts_tests[$host]) as $test)
                $grouptests[$test] = true;
        }
        $grouptests2 = array_keys($grouptests);
        // now choose all tests that match group rule
        $grouptests = array();
        foreach($grouptests2 as $test)
        {
            if (isset($pagearr["tests"]) && is_array($pagearr["tests"]))
            {
                if (in_array($test,$pagearr["tests"]))
                    $grouptests[$test] = true;
            }
            elseif (isset($pagearr["notests"]) && is_array($pagearr["notests"]))
            {
                if (!in_array($test,$pagearr["notests"]))
                    $grouptests[$test] = true;
            }
            else
                $grouptests[$test] = true;
        }
        $grouptests = array_keys($grouptests);
        sort($grouptests);
        //_dump($grouptests);


        // find hosts that have at least one test
        $new_hosts_tests = array();
        foreach($grouphosts as $host)
            foreach($grouptests as $test)
                if (isset($hosts_tests[$host][$test]))
                    $new_hosts_tests[$host][$test] = $hosts_tests[$host][$test];
        $grouphosts2 = array();
        foreach($grouphosts as $host)
            if (is_array($new_hosts_tests[$host]))
                $grouphosts2[]=$host;
        $grouphosts = $grouphosts2;
        $html .= make_table($grouphosts,$grouptests,$hosts_tests,$color);
    }

    return array($html,$color);
}


function make_page_groups_history($mode,$page,$spage)
{
    global $PHPSELF;
    list($xxx,$color) = make_page_groups($mode,$page);
    list($pagedata, $subpages, $parentpage) = get_page_data($page);
    $allhosts = get_hostnames();
    $hosts = array(); // only hosts in groups of the page
    foreach($pagedata["groups"] as $gp=>$pagearr)
    {
        foreach ($allhosts as $host)
        {
            if (!empty($hosts[$host]))
                continue;
            $skip = 1;
            foreach($pagearr["hosts"] as $hostre)
            {
                if ($hostre == $host)
                    $skip = 0;
                elseif (preg_match("/^[\/!].*[\/!]\w*$/",$hostre) && preg_match("$hostre",$host))
                    $skip = 0;
            }
            if ($skip)
                continue;
            $hosts[$host] = true;
        }
    }
    $hosts = array_keys($hosts);
    list($events,$nrecords) = get_all_events($hosts,$pagedata["tests"] ?? null, $pagedata["notests"] ?? null,$spage);
    $html = "";
    $html .= make_top_links($page,$mode);
    $html .= make_subpages_links($page,$mode,$subpages,$parentpage);
    $html .= make_any_history($events,$mode,"$page",$spage,$nrecords);
    return array($html,$color);
}

function make_top_links($page,$mode)
{
    global $PHPSELF;
    $html = "";
    $html .= ($page != "all") ? "<a class='page' href='$PHPSELF?m=$mode&p=all'>everything</a>"
                              : "<a class='page' href='$PHPSELF?m=$mode'>top page</a>";
    $html .= "\n";
    $html .= ($mode=="ng") ? "<a class='page' href='$PHPSELF?&p=$page'>page</a>"
                           : "<a class='page' href='$PHPSELF?m=ng&p=$page'>n/g+history</a>";
    $html .= "\n";
    return $html;
}

function make_subpages_links($page,$mode,$subpages,$parentpage)
{
    global $PHPSELF;
    $html = "| ";
    if ($parentpage !== null)
        $html .= "<a class='page' href='$PHPSELF?m=$mode&p='>^top</a> \n";
    if ($parentpage)
        $html .= "<a class='page' href='$PHPSELF?m=$mode&p=$parentpage'>$parentpage</a> \n";
    foreach($subpages as $sp)
        $html .= "<a class='page' href='$PHPSELF?m=$mode&p=$page/$sp'>$sp</a> \n";
    return $html;
}

function make_table($hosts,$tests,$hosts_tests,&$color)
{
    global $disabled;

    $hostcount = $testcount = 0;

    if ($color=="")
        $color = "green";
    $html = "<p><table border=1>\n<tr><td>Host</td>\n";
    foreach($tests as $t)
        $html .= "<td>".html($t)."</td>\n";
    $html .= "</tr>\n";
    foreach($hosts as $host)
    {
        $hostcount++;
        $html .= "<tr><td>".html($host)."</td>\n";
        foreach($tests as $test)
        {
            $c = $hosts_tests[$host][$test] ?? null;
            if ($c)
            {
                $testcount++;
                if (!empty($disabled[$host][$test]))
                {
                    $html .= "<td bgcolor='#006600'><a href='test.php?h=".urlencode($host)."&t=$test'>".html_icon($c)."</a></td>\n";
                }
                else
                {
                    $color = compile_color($color,$c);
                    $html .= "<td><a href='test.php?h=".urlencode($host)."&t=$test'>".html_icon($c)."</a></td>\n";
                }
            }
            else
                $html .= "<td>&nbsp;</td>\n";
        }
        $html .= "</tr>\n";
    }
    $html .= "</table>\n";
    $html .= "<div style='font-size: 12px'>hosts: $hostcount, tests: $testcount</div>\n";
    return $html;
}
