<?php

/*
    This is library for socket server.
    It's a more complex variant of acceptor.s

    omonServerClient extends socketServerClient
    omonServer extends socketServer
*/

require_once("socket.php");

function stop_daemon()
{
    global $PIDFILE;
    $pid = trim(@file_get_contents($PIDFILE));
    preg_match("/(\d+)/",$pid,$match);
    $pid = $match[1];
    if (!$pid)
    {
        echo "Error! No process to stop.\n";
        return;
    }

    $pspid = shell_exec("/bin/ps -p $pid");
    if (preg_match("/^(\d+)/m",$pspid,$match) && $pid == $match[1])
    {
        echo "Stopping server, pid $pid: ";
        posix_kill($pid,SIGINT);
        for ($i=0;$i<100;$i++)
        {
            usleep(50000);
            echo ".";
            if (!file_exists($PIDFILE))
            {
                echo " stopped\n";
                return;
            }
        }
        echo "Problem! Process still did not exit.\n";
    }
    else
    {
        echo "Server crashed last time!\n";
    }
    return;
}

function die_log($text)
{
    write_log($text);
    exit_daemon();
}

/*
function log_message($text)
{
    global $SRVLOG;
    global $NODETACH;
    $dt = date("r");
    $message = "$dt: $text\n";
    file_put_contents($SRVLOG,$message ,FILE_APPEND);
    if ($NODETACH)
        echo $message;
}
*/

function exit_daemon()
{
    global $PIDFILE;
    unlink($PIDFILE);
    exit;
}

function sig_handler($signo) {

    switch($signo) {
        case SIGTERM:
            // handle shutdown tasks
            die_log("Got SIGTERM, exiting");
            break;
        case SIGHUP:
            // handle restart tasks
            break;
        case SIGUSR1:
            //print "Caught SIGUSR1...\n";
            break;
        case SIGCHLD:
            while( pcntl_waitpid(-1,$status,WNOHANG)>0 ) {
            }
            break;
        case SIGINT:
            die_log("Got SIGINT, exiting");
            break;
        default:
            // not implemented yet...
            break;
     }
}


class omonServer extends socketServer {
}

class omonServerClient extends socketServerClient {
    private $max_total_time = 45;
    private $max_idle_time  = 15;
    private $accepted;
    private $last_action;

    var $rawdata;

    var $hostname;
    var $testname;
    var $timestamp;
    var $osname;
    var $processed = 0;

    private function handle_request()
    {
        global $NODETACH;
        if ($this->rawdata !="" && $this->hostname != "")
        {
            $ra = $_ENV["REMOTE_HOST"] = $this->remote_address;
            put_client_event($this->hostname,$this->testname,$this->rawdata,"","");
            $this->rawdata = "";
            //update_client_history($this->hostname,$this->testname);
            $this->processed++;
            if ($NODETACH) echo "*** got message for $this->hostname $this->testname from $ra\n";
            return "";
        }
        return "";
    }

    public function on_read()
    {
        global $NODETACH;
        $this->last_action = time();
        $buffer = $this->read_buffer;
        //echo "------------------------------------------------------------------\n";

        $lines = explode("\n",$buffer);
        //print_r($lines);
        $n_lines = count($lines)-1; // last line will go into buffer again to be processed next time

        for($i=0; $i<$n_lines;$i++)
        {
            $line = $lines[$i];
            //echo "*** $line\n";
            if (preg_match("/^\[info ([\w.-]+)\s+(\w+)\s+(\d{14})\s+(\w+)\]$/",$line,$match))
            {
                $this->write($this->handle_request());
                array_shift($match);
                list($this->hostname,$this->testname,$this->timestamp,$this->osname) = $match;
                $this->rawdata = $line."\n";
            }
            elseif ($line == "[-- end --]")
            {
                $this->read_buffer = join("\n",array_slice($lines,$i+1));
                $this->write($this->handle_request());
                if ($NODETACH) echo "[omonServerClient] {$this->remote_address} closing connection, accepted {$this->processed} messages\n";
                $this->close();
                omon_forward_requests();
                //foreach(array_keys($GLOBALS) as $var) if(!preg_match("/^(GLOBALS|_GET|_POST|_SERVER|_ENV|_COOKIE|HTTP_SERVER_VARS|HTTP_COOKIE_VARS|HTTP_ENV_VARS|HTTP_POST_VARS|HTTP_GET_VARS|_FILES|HTTP_POST_FILES|_REQUEST)$/",$var)) { echo "*** [$var] ***:\n"; print_r($GLOBALS[$var]); }
            }
            elseif ($line == "[-- shutdown --]" && $this->remote_address == "127.0.0.1")
            {
                die_log("got shutdown command, exiting");
            }
            else
                $this->rawdata .= $line."\n";
        }
        $new_buffer = array_pop($lines);
        //echo "new buffer: $new_buffer\n";
        $this->read_buffer  = $new_buffer;
    }

    public function on_connect()
    {
        global $NODETACH;
        if ($NODETACH) echo "[omonServerClient] accepted connection from {$this->remote_address}\n";
        $this->accepted    = time();
        $this->last_action = $this->accepted;
    }

    public function on_disconnect()
    {
        global $NODETACH;
        if ($NODETACH) echo "[omonServerClient] {$this->remote_address} disconnected\n";
    }

    public function on_write()
    {
        if (false && strlen($this->write_buffer) == 0) {
            $this->disconnected = true;
            $this->on_disconnect();
            $this->close();
        }
    }

    public function on_timer()
    {
        global $NODETACH;
        $idle_time  = time() - $this->last_action;
        $total_time = time() - $this->accepted;
        if ($total_time > $this->max_total_time || $idle_time > $this->max_idle_time)
        {
            if ($NODETACH) echo "[omonServerClient] Client keep-alive time exceeded ({$this->remote_address})\n";
            $this->close();
        }
    }
}

