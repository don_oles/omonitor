<?php

function write_log($text) {
    global $SERVERLOG;
    global $PHPSELF;
    $text = date("r")." $PHPSELF $text\n";
    file_put_contents($SERVERLOG,$text,FILE_APPEND);
    global $NODETACH;
    if ($NODETACH)
        echo $text;
}

function write_debug($text) {
    global $DEBUG;
    if (!$DEBUG)
        return;
    write_log($text);
}

function url($text) {
    return urlencode($text);
}

function html($t) {
    return htmlspecialchars($t);
}

function make_unix_timestamp($ts) {
    preg_match("/^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/",$ts,$match);
    list($xxx,$year,$month,$day,$hour,$min,$sec) = $match;
    return gmmktime($hour,$min,$sec,$month,$day,$year);
}

function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

function make_timestamp($ts=0) {
    if ($ts)
        return gmdate("YmdHis",$ts);
    return gmdate("YmdHis");
}

function make_timestamp2() {
    return date("YmdHis");
}

function _dump($var) {
    echo "<table border='1'><tr><td><pre>\n".
        htmlspecialchars(var_export($var,1)).
        "\n</pre></td></tr></table>\n";
}

function _dump2($x) {
    var_dump($x);
}

// this one copypasted from php manual forum
function shift_opts($options_array) {
    foreach( $options_array as $o => $a) {
        while($k=array_search("-".$o.$a,$GLOBALS['argv']))
            if($k)
                unset($GLOBALS['argv'][$k]);

        while($k=array_search("-".$o,$GLOBALS['argv']))
            if($k) {
                unset($GLOBALS['argv'][$k]);
                unset($GLOBALS['argv'][$k+1]);
            }
    }
    $GLOBALS['argv']=array_merge($GLOBALS['argv']);
}

// to remove?
function cut_data_lines($text)
{
    $cut = 160;
    $lines = preg_split("/[\r\n]/m",$text);
    //_dump($lines);
    $newlines = array();
    foreach($lines as $line)
        if (strlen($line)>$cut)
            $newlines[]=substr($line,0,$cut);
        else
            $newlines[]=$line;
    return join("",$newlines);
}

function time_diff_text($sec)
{
    $days = intval($sec/86400);
    $sec -= $days * 86400;
    $hours = intval($sec/3600);
    $sec -= $hours * 3600;
    $mins = intval($sec/60);
    $sec -= $mins * 60;
    $text = "";
    if ($days)  $text .= " $days days";
    if ($hours) $text .= " $hours hr";
    if ($mins)  $text .= " $mins min";
    if ($sec)   $text .= " $sec sec";
    return trim($text);
}

function html_icon($c) {
    global $ICON_URLS, $ICON_SIZE;
    $url = $ICON_URLS[$c] ?? 'icons/unknown.gif';
    return "<img src='$url' border='0' width='$ICON_SIZE'>";
}

