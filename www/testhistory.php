<?php

chdir(dirname(__FILE__)."/..");
require_once("lib/web.inc.php");
require_once("lib/omonitor.inc.php");
init_database();

$host = $_GET["h"];
$test = $_GET["t"];
$spage = $_GET['pp'] ?? 0;

check_hostname_syntax($host);
if (!preg_match("/^\w+$/",$test))
    die ("Bad test name");

$where = "WHERE hostname=".sqlsq($host)." AND testname=".sqlsq($test);

$rc = $SQL->query("SELECT status FROM current $where");
list($color) = $rc->fetch();

require("www/header.inc.php");

list($nrecords) = $SQL->query("SELECT COUNT(*) FROM history $where")->fetch();
$offset = $spage * $EVENTSHISTORYLIMIT;

$previous_timestamp = time();
if ($spage > 0) {
    $rc = $SQL->query("SELECT timestamp FROM history $where ORDER BY timestamp DESC LIMIT 1 OFFSET " . ($offset - 1));
    $row = $rc->fetch();
    preg_match("/^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/",$row[0],$match);
    list($xxx,$year,$month,$day,$hour,$min,$sec) = $match;
    $previous_timestamp = gmmktime($hour,$min,$sec,$month,$day,$year);
}

$events = array();
$rc = $SQL->query("SELECT timestamp,status FROM history $where ORDER BY timestamp DESC LIMIT $EVENTSHISTORYLIMIT OFFSET $offset");
while($row = $rc->fetch())
{
    list($ts,$c) = $row;
    if (!preg_match("/^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/",$ts,$match))
        continue;
    list($xxx,$year,$month,$day,$hour,$min,$sec) = $match;
    $ftime = gmmktime($hour,$min,$sec,$month,$day,$year);
    $events[$ftime] = array($c,$ts);
}

//krsort($events);
close_database();

$navhtml = "<table border=1><tr>\n";
$np = ceil($nrecords / $EVENTSHISTORYLIMIT);
for ($p=0;$p<$np && $p<$EVENTSHISTORYPAGELIMIT;$p++) {
    $navhtml .= "<td>";
    if ($p != $spage)
        $navhtml .= "<a href='$PHPSELF?h=$host&t=$test&pp=$p'>";
    $navhtml .= "$p";
    if ($p != $spage)
        $navhtml .= "</a>";
    $navhtml .= "</td>\n";
}
$navhtml .= "</tr></table>\n";


?>

<a class='page' href="./">top</a>

<table><tr>
<td><h1><?=html($host)?></h1></td>
<td><h2><a href='test.php?h=<?=url($host)?>&t=<?=url($test)?>'><?=html($test)?></h2></a></td>
</tr></table>

<?=$navhtml?>
<p>

<table border=1>
<tr>
    <td>Date</td>
    <td>Status</td>
    <td>Duration</td>
</tr>
<?php
foreach($events as $ts=>$arr)
{
    list($c,$event) = $arr;
    $diff = $previous_timestamp - $ts;

    $timediff = time_diff_text($diff);

    echo "<tr>\n";
    echo "<td>".html(date("y/m/d H:i:s",$ts))."</td>\n";
    echo "<td><a href='test.php?h=".urlencode($host)."&t=$test&e=$event'>".html_icon($c)."</a></td>";
    echo "<td>$timediff</td>\n";
    echo "</tr>\n";
    $previous_timestamp = $ts;
}

?>
</table>

<p>
<?=$navhtml?>

<?php

require("www/footer.inc.php");

