<?php

chdir(dirname(__FILE__)."/..");
require_once("lib/web.inc.php");
require_once("lib/omonitor.inc.php");
init_database();

$mode = $_GET['m'] ?? null;
$page = $_GET['p'] ?? null;
$spage = $_GET['pp'] ?? null;
//if ($page=="") $page = "/";

$disabled = get_disabled();

if ($page == "all")
{
    if ($mode == "ng")
        list($html,$color) = make_page_all_history($mode,$page,$spage);
    else
        list($html,$color) = make_page_all($mode);
}
else
{
    if ($mode == "")
        list($html,$color) = make_page_groups($mode,$page);
    else
        list($html,$color) = make_page_groups_history($mode,$page,$spage);
}

close_database();

require("www/header.inc.php");
echo $html;
require("www/footer.inc.php");

