<?php

chdir(dirname(__FILE__)."/..");
require_once("lib/web.inc.php");
require_once("lib/omonitor.inc.php");
init_database();

$host = $_GET["h"] ?? null;
$test = $_GET["t"] ?? null;
$event = $_GET["e"] ?? null;
$action = $_GET["a"] ?? null;
$dismode = $_GET['m'] ?? 0;

//_dump($_SERVER);

if ($action == "disable")
{
    disable_test($host,$test,$dismode);
    header("Location: $PHP_SELF?h=".url($host)."&t=".url($test));
    exit;
}
if ($action == "enable")
{
    enable_test($host,$test,1);
    header("Location: $PHP_SELF?h=".url($host)."&t=".url($test));
    exit;
}


check_hostname_syntax($host);
if (!preg_match("/^\w+$/",$test))
    die ("Bad test name");
if ($event && !preg_match("/^\d{14}$/",$event))
    die ("Bad event");

if ($event && preg_match("/^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/",$event,$match))
{
    $rc = $SQL->query("SELECT status,rawdata,description FROM history WHERE hostname=".sqlsq($host)." AND testname=".sqlsq($test)." AND timestamp=$event");
    $row = $rc->fetch();
    list($color,$data,$message) = $row;
    list($xxx,$year,$month,$day,$hour,$min,$sec) = $match;
    $ftime = gmmktime($hour,$min,$sec,$month,$day,$year);
    $eventtime = html(date("r",$ftime));
}
else
{
    $rc = $SQL->query("SELECT status,timestamp,rawdata,description FROM current WHERE hostname=".sqlsq($host)." AND testname=".sqlsq($test));
    list($color,$ts,$data,$message) = $rc->fetch();
    preg_match("/^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/",$ts,$match);
    list($xxx,$year,$month,$day,$hour,$min,$sec) = $match;
    $ftime = gmmktime($hour,$min,$sec,$month,$day,$year);
    $eventtime = date("r",$ftime);

    $rc = $SQL->query("SELECT timestamp,username,mode FROM disabled WHERE hostname=".sqlsq($host)." AND testname=".sqlsq($test));
    list($disabled_time,$disabled_username,$disabled_mode) = $rc->fetch();
    $disabled = ($disabled_time>0);
}

close_database();

require("www/header.inc.php");

$message = "*** Received at $eventtime\n$message";

$message = html($message);
$message = preg_replace_callback("/{(green|red|yellow|purple)}/",function($m){return html_icon($m[1]);},$message);
//$data = html(cut_data_lines($data));
$data = html($data);
$data = preg_replace("!(https?://\S+)!","<a class='page3' target=_blank href='\\1'>\\1</a>",$data);
?>

<a class='page' href="./">top</a>

<table><tr>
<td><h1><?=html($host)?></h1></td>
<td><h2><?=html($test)?></h2></td>
</tr></table>

<?php if($event): ?>
    <div style="text-decoration:blink">Event at <?=$eventtime?></div>
<?php else: ?>
    <?php if ($disabled): ?>
        <div>Disabled <?php if ($disabled_mode==1): ?>permanently<?php endif ?> at <?=html(date("r",$disabled_time))?> by '<?=html($disabled_username)?>'
        <a class='page2' href="<?=$PHPSELF?>?h=<?=url($host)?>&t=<?=url($test)?>&a=enable">enable</a></div>
    <?php else: ?>
        <div><a class='page2' href="<?=$PHPSELF?>?h=<?=url($host)?>&t=<?=url($test)?>&a=disable">disable</a>
        <a class='page2' href="<?=$PHPSELF?>?h=<?=url($host)?>&t=<?=url($test)?>&a=disable&m=1">disable permanently</a></div>
    <?php endif ?>
<?php endif ?>
<p>
<a class='page2' href="testhistory.php?h=<?=urlencode($host)?>&t=<?=$test?>">history</a>
<p>
<table border='1' class='status'><tr><td>
    <pre><?=$message?></pre>
</td></tr></table>
<br>
<table border='1' class='status'><tr><td>
    <div style='overflow: auto;width:1000px'>
    <pre><?=$data?></pre>
    <div>
</td></tr></table>

<?php
require("www/footer.inc.php");

