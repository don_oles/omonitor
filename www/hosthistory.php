<?php

chdir(dirname(__FILE__)."/..");
require_once("lib/web.inc.php");
require_once("lib/omonitor.inc.php");
init_database();

$host = $_GET["h"];
$test = $_GET["t"];

check_hostname_syntax($host);
if (!preg_match("/^\w+$/",$test))
    die ("Bad test name");

$rc = $SQL->query("SELECT status FROM current WHERE hostname=".sqlsq($host)." AND testname=".sqlsq($test));
list($color) = $rc->fetch();

require("www/header.inc.php");

$currtime = time();

$events = array();

$rc = $SQL->query("SELECT timestamp,status FROM history WHERE hostname=".sqlsq($host)." AND testname=".sqlsq($test). " ORDER BY timestamp DESC");
while($row = $rc->fetch())
{
    list($ts,$c) = $row;
    if (!preg_match("/^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/",$ts,$match))
        continue;
    list($xxx,$year,$month,$day,$hour,$min,$sec) = $match;
    $ftime = gmmktime($hour,$min,$sec,$month,$day,$year);
    $events[$ftime] = array($c,$ts);
}

//krsort($events);
close_database();

?>

<a class='page' href="./">top</a>

<table><tr>
<td><h1><?=html($host)?></h1></td>
<td><h2><a href='host.php?h=<?=url($host)?>&t=<?=url($test)?>'><?=html($test)?></h2></a></td>
</tr></table>

<table border=1>
<tr>
    <td>Date</td>
    <td>Status</td>
    <td>Duration</td>
</tr>
<?php
foreach($events as $ts=>$arr)
{
    list($c,$event) = $arr;
    $diff = $currtime - $ts;

    $timediff = time_diff_text($diff);

    echo "<tr>\n";
    echo "<td>".html(date("y/m/d H:i:s",$ts))."</td>\n";
    echo "<td><a href='host.php?h=".urlencode($host)."&t=$test&e=$event'>".html_icon($c)."</a></td>";
    echo "<td>$timediff</td>\n";
    echo "</tr>\n";
    $currtime = $ts;
}

?>
</table>
<?php

require("www/footer.inc.php");

