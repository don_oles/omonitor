<?php

function omon_prepare_tests_cpu($tests)
{
    $testdata = array();
    $y = 0.5;
    $r = 1.0;
    foreach($tests as $test)
    {
        if (preg_match("/^load (\S+) (\S+)$/mi",$test,$match))
        {
            list($xxx,$y,$r) = $match;
        }

    }
    $testdata = array($y,$r);
    return $testdata;
}

function omon_test_cpu($data,$testdata)
{

    $color = "clear";
    $message = "unable to test";
    list($y,$r) = $testdata;
    // load average: 0.45, 0.37, 0.27
    if (preg_match("/load averages?: (\S+), (\S+), (\S+)/",$data,$match))
    {
        $message = "";
        list($xxx,$la1,$la5,$la15) = $match;
        //_dump2("yellow: $y red: $r la5: $la5");
        if ($la5 < $y)
        {
            $color = "green";
        }
        elseif ($la5 < $r)
        {
            $color = "yellow";
        }
        else
        {
            $color = "red";
        }
    }
    //_dump2($data);_dump2($color); _dump2($message);
    //exit;
    return array($color,$message);
}

