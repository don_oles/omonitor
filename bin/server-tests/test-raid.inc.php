<?php

function omon_prepare_tests_raid($tests)
{
    return null; 
}

function omon_test_raid($data,$testdata)
{

    $color = "green";
    $message = "";
    $lines = preg_split("/$/m",$data);
    foreach ($lines as $line)
    {
        $line = trim($line);
        if (preg_match("!^/dev/cciss/\w+:.* status: (.*?)\.!",$line,$match)) // this is a CCISS smart array
        {
            $status = $match[1];
            if ($status == "OK")
                $cc = "green";
            elseif ($status == "FAILED")
                $cc = "red";
            else 
                $cc = "yellow";
            $message .= "{{$cc}} $line\n";
            $color = compile_color($color,$cc);
        }
        elseif (preg_match("!^ar[0-9]: .* status: (\w+)!",$line,$match)) // FreeBSD ata soft raid
        {
            $status = $match[1];
            if ($status == "READY")
                $cc = "green";
            elseif ($status == "BROKEN")
                $cc = "red";
            else 
                $cc = "yellow";
            $message .= "{{$cc}} $line\n";
            $color = compile_color($color,$cc);
        }
        elseif (preg_match("!^mirror/\w+\s+(\w+)!",$line,$match)) // FreeBSD ata soft raid
        {
            $status = $match[1];
            if ($status == "COMPLETE")
                $cc = "green";
            elseif ($status == "DEGRADED")
                $cc = "red";
            else 
                $cc = "yellow";
            $message .= "{{$cc}} $line\n";
            $color = compile_color($color,$cc);
        }
        elseif (preg_match("!Device\s+\d+\s+\(.*?\)\s+status\sis:\s(.*)!",$line,$match)) // Processed megacli output
        {
            $cc = "green";
            $status = $match[1];
            if ($status == "Offline")
                $cc = "red";
            $message .= "{{$cc}} $line\n";
            $color = compile_color($color,$cc);
        }
        // 0      1     ---    953865MB  300MB  74290E78  On-Line      SN06  1953515519  Seagate ST31000340NS
        elseif (preg_match("!\s+\d+MB\s+\d+MB\s+!",$line) || preg_match("!\s+---\s+---\s+---\s+---\s+!",$line) ) // Infortrend Eonstor 
        {
            $tokens = preg_split("/\s+/",trim($line));
            if ($tokens[6]=="Cloning")
            {
                $tokens[6] .= " ".$tokens[7];
                array_splice($tokens,7,1);
            }
            list($d_index,$d_slot,$xxx,$d_size,$d_speed,$d_ld,$d_status,$d_rev,$d_blocks,$d_model1,$d_model2) = $tokens;
            if (preg_match("/^(on-line|cloning|global|enclosure)/i",$d_status))
            {
                $cc = "green";
            }
            else
            {
                $cc = "red";
            }
            $message .= "{{$cc}} $d_slot $d_size $d_model1 $d_model2 - $d_status\n";
            $color = compile_color($color,$cc);
        }
        elseif (preg_match("!^(\w+)_(\w+):(\d+):(\d+):(\w+):(\w+):(\d+):(\d+):(\d+)!",$line,$match)) // dm-raid
        {
            // ddf1_raid1:488019072:128:mirror:ok:0:2:0
            /*
            /dev/sda:ddf1:ddf1_raid1:mirror:ok:488019106:0
            /dev/sdb:ddf1:ddf1_raid1:mirror:ok:488019106:0
            */
            $status = $match[6];
            if ($status == "ok")
                $cc = "green";
            else
                $cc = "red";
            $message .= "{{$cc}} $line\n";
            $color = compile_color($color,$cc);
        }
        elseif (preg_match("!da\d+\s+\(\s+\w+\)\s+RAID-\d+\s+(\w+)\s+\w+!",$line,$match)) // FreeBSD LSI MPT raid
        {
            $status = $match[1];
            if ($status == "OPTIMAL")
                $cc = "green";
            elseif ($status == "DEGRADED")
                $cc = "red";
            else
                $cc = "yellow";
            $message .= "{{$cc}} $line\n";
            $color = compile_color($color,$cc);
        }
        else
        {
            // $message .= "unrecognised: $line\n";
        }
    }
    return array($color,$message);
}


