<?php

function omon_prepare_tests_proc($tests)
{
    $testdata = array();
    foreach($tests as $test)
    {
        preg_match_all("/^proc (.+)$/mi",$test,$matches);
        //_dump($matches);
        //exit;
        $procs = $matches[0];
        foreach($procs as $proc)
        {
            $min = 1;
            $max = 0;
            $color = "red";
            // _dump($proc);

            if (preg_match("/^proc \"(.+?)\"(.*)$/i",$proc,$match))
            {
                $procname = $match[1];
                $text = $procname;
                $proc = $match[2];
            }
            elseif (preg_match("/^proc (\S+)(.*)$/i",$proc,$match))
            {
                $procname = $match[1];
                $text = $procname;
                $proc = $match[2];
            }
            // _dump($procname);
            // _dump($proc);

            list($_min,$_max,$_c) = explode(" ",trim($proc));

            if ($_min !== "")
                $min = $_min;

            if ($_max !== "")
                $max = $_max;

	    $_c = trim($c);
            if ($_c !== "")
                $color = $_c;

            if (preg_match("/text=\"(.*?)\"/i",$proc,$match))
                $text = $match[1];
            $testdata["$procname"] = array($min,$max,$color,$text);
        }
    }

    // _dump($testdata);
    return $testdata;
}

function omon_test_proc($data,$testdata)
{

    $color = "green";
    $message = "";
    $processes = preg_split("/$/m",$data);
    foreach($testdata as $proc=>$test)
    {
        list($min,$max,$pcolor,$text) = $test;
        $c = 0;
        $m = "";
        $maxtxt = ($max) ? "$max" : "unlimited";
        $newc = "green";
        if (preg_match("/^!.*!$/",$proc))
        {
            foreach($processes as $process)
                if (preg_match($proc,$process))
                    $c++;
        }
        else
        {
            foreach($processes as $process)
                if (stripos($process,$proc)!==false)
                    $c++;
        }

        if ($min && $c<$min)
        {
            $newc = $pcolor;
            $m = "{"."$newc} $text process count $c less then $min\n";
        }
        elseif ($max && $c>$max)
        {
            $newc = $pcolor;
            $m = "{"."$newc} $text process count $c greater then $maxtxt\n";
        }
        else
        {
            $m = "{"."$newc} $text process count $c between $min and $maxtxt\n";
        }
        if ($color == "green" && $newc == "yellow")
            $color = $newc;
        if ($newc == "red")
            $color = $newc;
        $message .= $m;
    }
    //_dump($color); _dump($message);
    return array($color,$message);
}


