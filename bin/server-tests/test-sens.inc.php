<?php

function omon_prepare_tests_sens($tests)
{
    $testdata = array();
    $y = 35;
    $r = 50;
    $hpasm_y_d = 20;
    $hpasm_r_d = 10;
    foreach($tests as $test)
    {
        if (preg_match("/^TEMP (\d+) (\d+)$/mi",$test,$match))
        {
            list($xxx,$y,$r) = $match;
        }
        if (preg_match("/^HPASM_SENS_TEMP_THRESHOLD (\d+) (\d+)$/mi",$test,$match))
        {
            list($xxx,$hpasm_y_d,$hpasm_r_d) = $match;
        }

    }
    $testdata = array($y,$r,$hpasm_y_d,$hpasm_r_d);
    return $testdata;
}

function omon_test_sens($data,$testdata)
{

    $color = "green";
    list($y,$r,$hpasm_y_d,$hpasm_r_d) = $testdata;
    $lines = preg_split("/$/m",$data);
    // CPU Temperature            49 C
    $message = "";
    foreach ($lines as $line) {
        $line = trim($line);
        if (preg_match("/(CPU|System)\s+Temperature\s+(\d+)\s+C/",$line,$match))
        {
            list($xxx,$xxx,$temperature) = $match;
            $c = "green";
            if ($temperature < $y)
            {
                $c = "green";
            }
            elseif ($temperature < $r)
            {
                $c = "yellow";
            }
            else
            {
                $c = "red";
            }
            $message .= "{"."$c} $line\n";
            $color = compile_color($color,$c);
        }
        if (preg_match("! Data for /dev/(\S+)!",$line,$match))
        {
            $tested_drive = $match[1];
        }
        if (preg_match("!(194|321) Temperature_Celsius!",$line,$match))
        {
            $values = preg_split("/\s+/",$line);
            $temperature = $values[9];
            $c = "green";
            if ($temperature < $y)
            {
                $c = "green";
                $message .= "{"."$c} $tested_drive $temperature<$y\n";
            }
            elseif ($temperature < $r)
            {
                $c = "yellow";
                $message .= "{"."$c} $tested_drive $temperature>=$y\n";
            }
            else
            {
                $c = "red";
                $message .= "{"."$c} $tested_drive $temperature>=$r\n";
            }
            $color = compile_color($color,$c);
        }
        if (preg_match("!(\S+)\s+(\d+)C/\d+F\s+(\d+)C/\d+F!",$line,$match))
        {
            list($xxx,$sensor,$temperature,$threshold) = $match;
            $y = $threshold - $hpasm_y_d;
            $r = $threshold - $hpasm_r_d;
            $c = "green";
            if ($temperature < $y)
            {
                $c = "green";
                $message .= "{"."$c} $sensor $temperature<$threshold-$hpasm_y_d\n";
            }
            elseif ($temperature < $r)
            {
                $c = "yellow";
                $message .= "{"."$c} $sensor $temperature>=$threshold-$hpasm_y_d\n";
            }
            else
            {
                $c = "red";
                $message .= "{"."$c} $sensor $temperature>=$threshold-$hpasm_r_d\n";
            }
            $color = compile_color($color,$c);
        }

    }
    return array($color,$message);
}


