<?php

function omon_prepare_tests_serv($tests)
{
    $testdata = array();
    // var_dump($tests);
    foreach($tests as $test)
    {
        preg_match_all("/^serv (.+)$/mi",$test,$matches);
        //var_dump($matches);
        //exit;
        $servs = $matches[0];
        foreach($servs as $serv)
        {
            if (preg_match("/^serv (.*)$/i",$serv,$match))
            {
                $servname = $match[1];
            }
            $testdata = array_merge($testdata,explode(" ",$servname));
        }
    }
    return $testdata;
}

function omon_test_serv($data,$testdata)
{
    $color = "green";
    $message = "";
    $services = preg_split("/$/m",$data);
    foreach($testdata as $serv)
    {
        //echo "serv $serv\n";
        $cc = "green";
        foreach($services as $service_line) 
        {
            $service_line = trim($service_line);
            //echo "line: $service_line\n";
            list($servname,$servmode,$servstatus) = preg_split("/\s+/",$service_line);
            // echo "line: $servname $servmode $servstatus\n";
            if ($servname == $serv)
            {
                // echo "line: $servname $servmode $servstatus\n";
                if ($servstatus == "Running")
                    $cc = "green";
                else
                    $cc = "red";
                $message .= "{{$cc}} $servname $servstatus\n";
                $color = compile_color($color,$cc);
            }
        }
    }
    // var_dump($color); var_dump($message);
    return array($color,$message);
}


