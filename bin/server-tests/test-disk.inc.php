<?php

function omon_prepare_tests_disk($tests)
{
    $testdata = array();
    foreach($tests as $test)
    {
        preg_match_all("/^disk (\S+) (\d{1,3}) (\d{1,3})$/mi",$test,$matches);
        //_dump($matches);
        for($i=0;$i<count($matches[0]);$i++)
        {
            $disk = $matches[1][$i];
            $ylev = $matches[2][$i];
            $rlev = $matches[3][$i];
            $testdata["$disk"] = array($ylev,$rlev);
        }
    }

    //_dump($testdata);
    return $testdata;
}

function omon_test_disk($data,$testdata)
{
    //_dump($testdata);
    //_dump($data);
    $mounts = preg_split("/$/m",$data);
    //_dump($mounts);
    $color = "green";
    $message = "";
    foreach($mounts as $mount)
    {
        $c = "green";
        $m = "";
        $mount = trim($mount);
        if (!preg_match("/^(\S+).*\s+(\d+)%\s+(\S+)$/",$mount,$match))
            continue;
        list($xxx,$fs,$level,$mp) = $match;
        if (preg_match("!^(linprocfs|tmpfs|devfs|procfs|udev|shm|.+\.iso|/dev/loop\d+)$!i",$fs))
            continue;
        if (preg_match("!^\d+\.\d+\.\d+\.\d+:/!i",$fs))
            continue;
        if (is_array($testdata["$mp"]))
            list($ylev,$rlev) = $testdata["$mp"];
        else
            list($ylev,$rlev) = $testdata["*"];
        if ($level >= $ylev)
            $c = "yellow";
        if ($level >= $rlev)
            $c = "red";
        $m = "{"."$c} mountpoint: $mp ${level}% (yellow:${ylev}% red:${rlev}%)\n";
        //if ($color == "green" && $c == "yellow") $color = $c;
        //if ($c == "red") $color = $c;
        $color = compile_color($color,$c);
        $message .= $m;
    }
    //_dump($color); _dump($message);
    return array($color,$message);
}


