<?php
/*
    Create file test-<test>.inc.php and put it here.
    It has to have two functions.
*/

function omon_prepare_tests_<test>($tests)
{
    /*
        Function omon_prepare_tests_<test> is given array of
        preprocessed 'etc/clients' pieces that correspond to
        the checked host. So, for example, disk checks may
        to parse it looking for 'DISK /mp X Y' strings.
    */
    //.................
    return $preapred_test_data;
}

function omon_test_<test>($data,$preapred_test_data)
{
    /*
        Function omon_test_<test> is given the data to check
        and the prepared by omon_prepare_tests_<test> configuration
        data. Result is status color and a description of test result.
        You may use {<color>} in message like {green} or {red}, these
        will be presented by web-interface as color dots.
    */
    //.................
    return array($color,$message);
}

