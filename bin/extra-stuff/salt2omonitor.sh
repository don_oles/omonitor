#!/bin/sh

# A sample file. Must be run from cron by root user (the one able to run salt)

TMP="/tmp/salt-omon-data$$"
/bin/rm -f $TMP
TESTDATE=`/bin/date -u +"%Y%m%d%H%M%S"`
/usr/bin/salt -G 'kernel:Linux' cmd.run "df -P -k" >>$TMP
/usr/bin/perl -p -i -e "s/^(\S+):\$/[info \$1 disk ${TESTDATE} Linux]/g" $TMP
/usr/bin/salt -G 'kernel:Linux' cmd.run "/bin/ps -Aw -o pid,ppid,user,start,state,pri,pcpu,time,pmem,rsz,vsz,cmd" >>$TMP
/usr/bin/perl -p -i -e "s/^(\S+):\$/[info \$1 proc ${TESTDATE} Linux]/g" $TMP
/usr/bin/salt -G 'kernel:Linux' cmd.run "uptime" >>$TMP
/usr/bin/perl -p -i -e "s/^(\S+):\$/[info \$1 cpu ${TESTDATE} Linux]/g" $TMP
echo "[-- end --]" >>$TMP

#echo "=== Feeding data"
# /bin/cat ${TMP} | /srv/omonitor/bin/acceptor
/bin/cat ${TMP} | /bin/nc -w 20 localhost 1975

/bin/rm -f $TMP
