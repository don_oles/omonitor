<?php
/*
    This script is run from xinetd or inetd.
    Accepts all n_messages from stdin and puts them to omonitor
    engine.
*/

chdir(dirname(__FILE__)."/..");
require_once("lib/omonitor.inc.php");

$fd = fopen("php://stdin","r");

$hostname = "";
$data = "";
$messages = array();
$timestamp = make_timestamp();

while(!feof($fd))
{
    $line = rtrim(fgets($fd));
    $proper = preg_match("/^\[info ([\w.-]+)\s+(\w+)\s+(\d{14})\s+(\w+)\]$/",$line,$match);
    if ($proper)
    {
        if ($data !="" && $hostname != "")
        {
            $messages[] = array($hostname,$testname,$data,$timestamp);
        }
        $data = "";

        list($xxx,$hostname,$testname,$timestamp,$osname) = $match;
        write_debug("got header $hostname $testname $timestamp $osname");

        if ($timestamp=="00000000000000")
            $timestamp = make_timestamp();
        $hostname = strtolower($hostname);

        check_hostname_syntax($hostname);
    }
    if ($line == "[-- end --]")
    {
        write_debug("got end line");
        break;
    }
    $data .= "$line\n";
}
fclose($fd);
if ($data !="" && $hostname != "")
{
    $messages[] = array($hostname,$testname,$data,$timestamp);
}
init_database();
foreach($messages as $mess)
{
    list($hostname,$testname,$data,$timestamp) = $mess;
    put_client_event($hostname,$testname,$data,"","",$timestamp);
}
close_database();
$n_messages = count($messages);

//echo "Finished, got $n_messages messages\n";

omon_forward_requests();
