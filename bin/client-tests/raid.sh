if [ -f "${CCISS_VOL_STATUS}" ]
then
  eval ${SUDO} ${CCISS_VOL_STATUS} ${CCISS_VOLS}
fi

if [ -f "${ATACONTROL}" ]
then
if ls /dev/ar[0-9] >/dev/null 2>&1
then
  for i in /dev/ar[0-9] ; do
    i=${i##/dev/}
    eval ${SUDO} ${ATACONTROL} status ${i}
    # echo eval ${SUDO} ${ATACONTROL} status ${i}
  done
fi
fi

if [ -f "${GMIRROR}" ]
then
  eval ${SUDO} ${GMIRROR} status 2>/dev/null
fi

if [ -f "${MPTUTIL}" -a -c /dev/mpt0 ]
then
  eval ${SUDO} ${MPTUTIL} show volumes 2>/dev/null
fi

if [ -f "${MEGACLI}" ]
then
  eval ${SUDO} ${MEGACLI} -PDList -aALL 2>/dev/null | awk -f lib/megacli.awk 
fi

if [ -f "${DMRAID}" ]
then
  eval ${SUDO} ${DMRAID} -s -c -c -c
fi
