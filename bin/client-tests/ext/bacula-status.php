<?php

require_once("etc/client.inc.php");

if (!is_array($BACULA_IGNORE_JOBS))
    $BACULA_IGNORE_JOBS = array();

$color = "green";
$message = "";
$data = "";

if (!preg_match("/^(\w+):([^@]+)@([^\/]+)\/(.+)$/",$BACULA_DATABASE,$match))
{
    echo "\n[status red]\ncan not parse BACULA_DATABASE\n";
    exit;
}
list($xxx,$user,$password,$host,$database) = $match;
$link = mysql_connect("$host","$user","$password");
if (!$link)
{
    echo "\n[status red]\ncan not connect to $host as $user\n";
    exit;
}
$rc = mysql_select_db($database,$link);
if (!$rc)
{
    echo "\n[status red]\ncan not select database $database\n";
    exit;
}

$clients = array();
$rc = mysql_query("SELECT ClientId,Name,Uname FROM Client");
while($row = mysql_fetch_row($rc))
{
    list($id,$name,$uname) = $row;
    $clients[$id] = array($name,$uname);
}

$statuses = array();
$rc = mysql_query("SELECT JobStatus,JobStatusLong FROM Status");
while($row = mysql_fetch_row($rc))
{
    list($id,$name) = $row;
    $statuses[$id] = $name;
}

$jobs = array();
$yesterday = time() - 60 * 60 * 24 * 7;
$rc = mysql_query("SELECT Name, JobId FROM Job WHERE Type='B' AND JobTDate>$yesterday AND EndTime<>'0000-00-00 00:00:00' ORDER BY Name, JobTDate DESC");
while($row = mysql_fetch_row($rc))
{
    list($jobname, $jobid) = $row;
    if (in_array($jobname,$BACULA_IGNORE_JOBS)) continue;
    if (!$jobs[$jobname]) $jobs[$jobname] = $jobid;
}

foreach ($jobs as $jobname=>$id)
{
    $rc = mysql_query("SELECT * , UNIX_TIMESTAMP(EndTime) - UNIX_TIMESTAMP(StartTime) as time_diff FROM Job WHERE JobId=$id");
    $row = mysql_fetch_assoc($rc);
    // var_dump($row);
    // $clientid = $row["ClientId"]; $client = $clients[$clientid][0]." ".$clients[$clientid][1];
    $clientid = $row["ClientId"]; $client = $clients[$clientid][1];
    $jobstatus = $row["JobStatus"]; $jobstatusfull = $statuses[$jobstatus];
    $job= $row["Job"];
    $level = $row["Level"];
    $bytes = $row["JobBytes"] + 0; $kbytes = (int) ($bytes/1024); $mbytes = (int)($kbytes/1024); $gbytes = (int)($mbytes/1024);
    // var_dump("$bytes,$kbytes,$mbytes,$gbytes");
    // $size = ($gbytes) ? "${gbytes}G" : (($mbytes) ? "${mbytes}M" : ($kbytes) ? "${kbytes}K" : "${bytes}");
    $timediff = $row["time_diff"];
    $endtime = $row["EndTime"];
    $speed = ($timediff) ?  (int)($bytes/$timediff/1024) : 0;

    if ($jobstatus == "T")
    {
        $message .= "{green} $endtime $job, size: ${kbytes}K, ${speed}KB/s\n";
    }
    else
    {
        $cc = "red";
        if (preg_match("/-pc$/",$jobname))
            $cc = "yellow";
        $color = compile_color($color,$cc);
        $message .= "{{$cc}} $endtime $job $jobstatusfull\n";
        $rc2 = mysql_query("SELECT LogText,`Time` FROM Log WHERE JobId=$id ORDER BY LogId");
        while($row2 = mysql_fetch_row($rc2))
        {
            $text = trim($row2[0]);
            $time = $row2[1];
            if (!preg_match("/Elapsed time:/",$text))
                $data .= "$time: $text\n";
        }
        $data .= "----------------------------------------------------\n";
    }
}


echo "$data\n";
echo "[status $color]\n";
echo "$message\n";

//------------------------------

function compile_color($c1,$c2)
{
    if ($c1 == "red" || $c2 == "red")
        return "red";
    if ($c1 == "yellow" || $c2 == "yellow")
        return "yellow";
    if ($c1 == "purple" || $c2 == "purple")
        return "purple";
    return "green";
}


