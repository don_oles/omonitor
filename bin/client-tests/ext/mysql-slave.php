<?php

require_once("etc/client.inc.php");

$color = "green";
$message = "";
$data = "";
if (is_array($MYSQLSLAVESERVERS)) foreach($MYSQLSLAVESERVERS as $srv)
{
    $c = "green";
    list($desc,$host,$port,$user,$pass) = $srv;
    $link = mysql_connect("$host:$port","$user","$pass");
    if (!$link)
    {
        $color = compile_color($color,"red");
        $message .= "{red} $desc: can not connect\n";
        continue;
    }
    // echo "$desc\n";
    $$Slave_IO_Running="";
    $rc = mysql_query("SHOW SLAVE STATUS",$link);
    $row = mysql_fetch_array($rc);
    if (!is_array($row))
    {
        $color = compile_color($color,"red");
        $message .= "{red} $desc: slave not configured\n";
        continue;
    }
    extract($row);

    $Relay_Log_Space = round($Relay_Log_Space/1048576,2);

    $data .= "*** $desc: $Slave_IO_State/$Slave_IO_Running/$Slave_SQL_Running
${Master_User}@${Master_Host}:$Master_Port $Replicate_Do_DB/$Replicate_Ignore_Table
$Master_Log_File:$Read_Master_Log_Pos $Relay_Log_File:$Relay_Log_Pos $Exec_Master_Log_Pos/$Seconds_Behind_Master ${Relay_Log_Space}Mb
$Last_Error

";
    if ($Slave_IO_Running != "Yes")
    {
        $c = "red";
        $message .= "{red} $desc: Slave_IO_Running $Slave_IO_Running\n";
    }
    if ($Slave_SQL_Running != "Yes")
    {
        $c = "red";
        $message .= "{red} $desc: Slave_SQL_Running $Slave_SQL_Running\n";
    }
    $color = compile_color($color,$c);
    if ($c == "green")
        $message .= "{green} $desc: $Slave_IO_State\n";
    mysql_close($link);
    
}
echo "$data\n";
echo "[status $color]\n";
echo "$message\n";

//------------------------------

function compile_color($c1,$c2)
{
    if ($c1 == "red" || $c2 == "red")
        return "red";
    if ($c1 == "yellow" || $c2 == "yellow")
        return "yellow";
    if ($c1 == "purple" || $c2 == "purple")
        return "purple";
    return "green";
}


