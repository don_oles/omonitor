_FETCHLOG_FILES=""
_FL_TMP="${OHOME}/tmp/fl.result.tmp"
_FL_MESSAGE="${OHOME}/tmp/fl.message.tmp"
_FL_COLOR_RESULT="green"

${RM} -f ${_FL_MESSAGE}

_FL_RECS=`set|${GREP} FETCHLOG_FILE_|${CUT} -d "=" -f 1`
for i in $_FL_RECS
do
    set -- `eval echo \\$$i`
    _FL_FILE=$1; shift
    _FL_COLOR=$1; shift
    _FL_IGNORE=$1; shift
    _FL_RE=$*
    
    if [ "${_FL_IGNORE}" = "" -o  "${_FL_IGNORE}" = "-" ]
    then
        _FL_IGNORE="thereisnosuchstring"
    fi
    
    _FL_BOOKMARK=`echo ${_FL_FILE}|${MD5}`
    _FL_MD5_VAR="_FL_MD5_VARIABLE_${_FL_BOOKMARK}"
    _FL_FOUND=`eval echo \\$$_FL_MD5_VAR`
    if [ "$_FL_FOUND"  = "" ]
    then
        _FETCHLOG_FILES="$_FETCHLOG_FILES $_FL_FILE"
        setvar $_FL_MD5_VAR found
    fi

    _FL_BOOKMARK="${OHOME}/tmp/fl.${_FL_BOOKMARK}.tmp"
    eval "${FETCHLOG} -f 1:100:9999: ${_FL_FILE} ${_FL_BOOKMARK} ${_FL_RE}|${GREP} -v ${_FL_IGNORE} >${_FL_TMP}"

    # if [ ! -s "${_FL_TMP}" ] 
    _FL_DATA=`${CAT} ${_FL_TMP}`
    OIFS=$IFS
    IFS='
'
    for line in ${_FL_DATA}
    do
        echo {$_FL_COLOR} ${_FL_FILE}: $line >>${_FL_MESSAGE}
        if [ "$_FL_COLOR" = "red" ]
        then
            _FL_COLOR_RESULT="red"
        fi
        if [ "$_FL_COLOR" = "yellow"  -a "${_FL_COLOR_RESULT}" = "green" ]
        then
            _FL_COLOR_RESULT="yellow"
        fi
    done
    IFS=$OIFS
done

for i in ${_FETCHLOG_FILES}
do
    _FL_BOOKMARK=`echo ${i}|${MD5}`
    _FL_BOOKMARK="${OHOME}/tmp/fl.${_FL_BOOKMARK}.tmp"
    eval "${FETCHLOG} -F 1:100:9999: ${i} ${_FL_BOOKMARK}"
done

echo "scanning files: ${_FETCHLOG_FILES}"
echo "[status ${_FL_COLOR_RESULT}]"
${CAT} ${_FL_MESSAGE}

#set