# echo $SENSORS
if [ -x /usr/local/sbin/bsdhwmon ]
then
    sudo /usr/local/sbin/bsdhwmon
fi
if [ -f /sbin/hpasmcli -o -h /sbin/hpasmcli ]
then
    sudo /sbin/hpasmcli -s "show temp"
fi

if [ -x /usr/local/sbin/smartctl ]
then
    for i in `ls /dev/ad*|egrep "ad[[:digit:]]+$"`; do
        echo === Data for $i
        sudo /usr/local/sbin/smartctl -A $i
    done
fi
