<?php

chdir(dirname(__FILE__)."/..");
require_once("lib/omonitor.inc.php");

$defaulthost = 'debug.test.host';
$defaulttest = 'dbg';
$valid_colors = "green,yellow,red,purple,blue,clear";

$options = getopt("h:t:c:r");
$color = $options["c"] ?? null;

$host = $options["h"] ?? $defaulthost;
$test = $options["t"] ?? $defaulttest;

if (isset($options["r"]) && $options["r"]===false) {
    init_database();
    remove_host($host);
    echo "Removed all events for host $host\n";
} elseif (in_array($color,preg_split("/,/",$valid_colors))) {
    init_database();
    put_client_event($host,$test,"It's a test alert {{$color}}\nhttps://www.youtube.com/\n",$color,"It's a test message for the {{$color}} alert");
} else {
    echo <<<EOF
Usage: 

php ${argv[0]} -c <color> [-h <hostname>] [-t <testname>] | -r

-c <color>          put alert of <color>, which can be one of $valid_colors
-h <hostname>       operate on <hostname> instead of '${defaulthost}'
-t <testname>       operate on <testname> instead of '${defaulttest}'
-r             

EOF;
}

