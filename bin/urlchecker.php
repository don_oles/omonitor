<?php
/*
    This script must by run by server periodically.
    Checks all URL's in 'clients', downloading those pages
    and searching for given text/pcre.
    Reports URL availability directly to omonitor engine.
*/

chdir(dirname(__FILE__)."/..");
require_once("lib/omonitor.inc.php");

function get_all_urls() {
    global $CERTIFICATE_EXPIRATION_YELLOW_DAYS,$CERTIFICATE_EXPIRATION_RED_DAYS;
    $urls = array();
    $all_clients = get_clients();
    foreach($all_clients as $client) {
        list($host,$tests) = $client;
        if (!preg_match("/^\w[\w.-]+\w$/",$host))
            continue;
        $tests = explode("\n",$tests);
        foreach($tests as $test) {
            if (!preg_match("!url (\S+) (\w+://\S+)\s(\S+)(.*)$!i",$test,$match))
                continue;
            list($xxx,$title,$url,$greenre,$extra) = $match;
            $yellowre = "";
            if (preg_match("!yellow=(\S+)!i",$extra,$match))
                $yellowre = $match[1];
            $redre = "";
            if (preg_match("!red=(\S+)!i",$extra,$match))
                $redre = $match[1];
            list($certyellow,$certred) = array($CERTIFICATE_EXPIRATION_YELLOW_DAYS,$CERTIFICATE_EXPIRATION_RED_DAYS);
            if (preg_match("!cert_exp=(-?\d+),(-?\d+)!i",$extra,$match)) {
                list($xxx,$certyellow,$certred) = $match;
            }
            $userpwd = '';
            if (preg_match("/^(\w+:\/\/)(\S+:\S+)@(\S+)/",$url,$match)) {
                $url = $match[1].$match[3];
                $userpwd = $match[2];
            }
            $urls[] = array($host,$url,$greenre,$yellowre,$redre,$title,$userpwd,$certyellow,$certred);
        }
    }
    return $urls;
}

$urls = get_all_urls();

$multi = curl_multi_init();
$channels = array();
foreach($urls as $urlarr) {
    list($host,$url,$greenre,$yellowre,$redre,$title,$userpwd,$certyellow,$certred) = $urlarr;
    $cookie_file = "tmp/cookies-".md5($url).".txt";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_CERTINFO, true);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
    curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate'); 
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    //curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY ) ;
    //curl_setopt($ch, CURLOPT_USERPWD, $userpwd);
    //curl_setopt($ch, CURLOPT_PROXY, 'proxy_ip:proxy_port');
    curl_multi_add_handle($multi, $ch);
    $channels[] = array($urlarr,$ch);
}

$active = null;
do { $mrc = curl_multi_exec($multi, $active); } while ($mrc == CURLM_CALL_MULTI_PERFORM);
while ($active && $mrc == CURLM_OK) {
    usleep(100000);
    do { $mrc = curl_multi_exec($multi, $active); } while ($mrc == CURLM_CALL_MULTI_PERFORM);
}

$colors = $colors_x = $messages = $messages_x = $datas = $datas_x = array();
foreach ($channels as $charr) {
    list($urlarr,$ch) = $charr;
    list($host,$url,$greenre,$yellowre,$redre,$title,$userpwd,$certyellow,$certred) = $urlarr;

    $colors[$host]   = $colors[$host]   ?? "green";
    $messages[$host] = $messages[$host] ?? "";

    $data = curl_multi_getcontent($ch);
    $transfer_info = curl_getinfo($ch);
    $certinfo = $transfer_info['certinfo'][0] ?? null;
    $cert_subj = $certinfo['Subject'];
    $cert_expire = $cert_issuer = null;
    if (!empty($cert_subj)) {
        $cert_expire_str = $certinfo['Expire date'];
        $cert_issuer = $certinfo['Issuer'];
        $cert_expire = (int)((strtotime($cert_expire_str) - time ())/86400);
        $colors_x[$host]   = $colors_x[$host]   ?? "green";
        $messages_x[$host] = $messages_x[$host] ?? "";
    }
    $errmess = curl_error($ch);

    $color = "red";
    $message = "";
    if ($data == "" || $errmess != "") {
        $color = "red";
        $message = "{red} $title got: $errmess";
    } elseif ($greenre != "//" && !preg_match($greenre,$data)) {
        $color = "red";
        $message = "{red} $title not found $greenre";
    } elseif ($redre != "//" && $redre != "" && preg_match($redre,$data)) {
        $color = "red";
        $message = "{red} $title found $redre";
    } elseif ($yellowre != "//" && $yellowre != "" && preg_match($yellowre,$data)) {
        $color = "yellow";
        $message = "{yellow} $title found $yellowre";
    } elseif ($greenre != "//") {
        $color = "green";
        $message = "{green} $title found $greenre";
    } else {
        $color = "green";
        $message = "{green} $title OK";
    }
    $colors[$host] = compile_color($colors[$host],$color);

    $cert_info = $message_x = "";
    if (!empty($cert_subj)) {
        $color_x = "green";
        if ($cert_expire <= $certred) {
            $color_x = "red";
        } elseif ($cert_expire <= $certyellow) {
            $color_x = "yellow";
        }
        $colors_x[$host] = @compile_color($colors_x[$host],$color_x);
        $message_x = "{"."$color_x} $title x509 expires:$cert_expire_str remains:$cert_expire days (y:$certyellow r:$certred)";
        $messages_x[$host] .= "$message_x\n";
        if (!isset($datas_x[$host]))
            $datas_x[$host] = "";
        $data_x = "URL: $url\nSubject: $cert_subj\nIssuer:$cert_issuer\nExpires: $cert_expire_str\nRemains: $cert_expire days (threshold $certyellow/$certred)\n\n";
        $datas_x[$host] .= $data_x;
    }

    $messages[$host] .= "$message\n";
    if (!isset($datas[$host]))
        $datas[$host] = "";
    $data_u = "URL: $url\nData size: ".strlen($data)."\nTotal time:".$transfer_info['total_time']."\n\n";
    $datas[$host] .= $data_u;
    curl_multi_remove_handle($multi, $ch);
}
curl_multi_close($multi);

init_database();
foreach($colors as $host=>$color) {
    $message = $messages[$host];
    $data = $datas[$host];
    put_client_event($host,'url',$data,$color,$message);
}
foreach($colors_x as $host=>$color) {
    $message = $messages_x[$host];
    $data = $datas_x[$host];
    put_client_event($host,'x509',$data,$color,$message);
}
close_database();
omon_forward_requests();

