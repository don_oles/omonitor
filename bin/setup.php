<?php

chdir(dirname(__FILE__)."/..");
require_once("lib/omonitor.inc.php");

check_for_modules();

//if ($DATABASE == "")
//    die ("Please setup \$DATABASE $DATABASE  in server.inc.php\n");

@list($xxx,$arg1,$arg2,$arg3) = $argv;

//-------------------------------------------------------------------
if ($arg1 == "init")
{
    if (preg_match("/^sqlite.?:/i",$PDO_DSN) && file_exists($DATABASE))
        die ("Error! File for database $DATABASE already exists!\n");
    init_database(true);
$sqls=explode("\n",trim("
CREATE TABLE current (hostname VARCHAR(128) NOT NULL DEFAULT '', testname VARCHAR(64) NOT NULL DEFAULT '', timestamp BIGINT DEFAULT 0, status VARCHAR(16) NOT NULL DEFAULT '', rawdata TEXT NOT NULL, description TEXT NOT NULL);
CREATE UNIQUE INDEX i1 ON current (hostname,testname);
CREATE TABLE history (hostname VARCHAR(128) NOT NULL DEFAULT '', testname VARCHAR(64) NOT NULL DEFAULT '', timestamp BIGINT DEFAULT 0, timestamp_prev BIGINT DEFAULT 0, status VARCHAR(16) NOT NULL DEFAULT '', status_prev VARCHAR(16) NOT NULL DEFAULT '',  rawdata TEXT NOT NULL, description TEXT NOT NULL);
CREATE UNIQUE INDEX i2 ON history (hostname,testname, timestamp);
CREATE TABLE disabled (hostname VARCHAR(128) NOT NULL DEFAULT '', testname VARCHAR(64) NOT NULL DEFAULT '', timestamp BIGINT DEFAULT 0, username VARCHAR(255) NOT NULL DEFAULT '', mode SMALLINT DEFAULT 0);
CREATE UNIQUE INDEX i3 ON disabled (hostname,testname);
CREATE TABLE alerts (hostname VARCHAR(128) NOT NULL DEFAULT '', testname VARCHAR(64) NOT NULL DEFAULT '', recipient VARCHAR(128) NOT NULL DEFAULT '', timestamp BIGINT DEFAULT 0, color VARCHAR(16) NOT NULL DEFAULT '');
CREATE UNIQUE INDEX i4 ON alerts (hostname,testname,recipient);
"));
    foreach($sqls as $sql) {
        print "=== Executing: ${sql}\n";
        $SQL->exec($sql);
        print_r($SQL->errorInfo());
    }
    echo "Database $DATABASE created.\n";
}
elseif ($arg1 == "disable")
{
    $host = $arg2;
    $test = $arg3;
    if ($host == "")
        die ("Error! Provied hostname\n");
    if ($test == "")
        die ("Error! Provied testname\n");
    init_database();
    $is = disable_test($host,$test,1);
    echo "Host $host test $test disabled\n";
}
elseif ($arg1 == "isdisabled")
{
    $host = $arg2;
    $test = $arg3;
    if ($host == "")
        die ("Error! Provied hostname\n");
    if ($test == "")
        die ("Error! Provied testname\n");
    init_database();
    $is = is_disabled($host,$test);
    echo "Host $host test $test disabled:$is\n";
}
elseif ($arg1 == "rmtest")
{
    $host = $arg2;
    $test = $arg3;
    if ($host == "")
        die ("Error! Provied hostname\n");
    if ($test == "")
        die ("Error! Provied testname\n");
    init_database();
    remove_host_test($host,$test);
    echo "Removed all events for host $host and test $test.\n";
}
elseif ($arg1 == "rmhost")
{
    $host = $arg2;
    if ($host == "")
        die ("Error! Provied hostname\n");
    init_database();
    remove_host($host);
    echo "Removed all events for host $host.\n";
}
elseif ($arg1 == "hosthist")
{
    $host = $arg2;
    if ($host == "")
        die ("Error! Provied hostname\n");
    init_database();
    foreach($SQL->query("SELECT testname,status,timestamp FROM history WHERE hostname=".sqlsq($host)." ORDER BY timestamp DESC") as $row)
    {
        extract($row);
        echo "$timestamp $testname $status\n";
    }
}
elseif ($arg1 == "testhist")
{
    $host = $arg2;
    $test = $arg3;
    if ($host == "")
        die ("Error! Provied hostname\n");
    if ($test == "")
        die ("Error! Provied testname\n");
    init_database();
    foreach($SQL->query("SELECT status,timestamp FROM history WHERE hostname=".sqlsq($host)." AND testname=".sqlsq($test)." ORDER BY timestamp DESC") as $row)
    {
        extract($row);
        echo "$timestamp $status\n";
    }
}
elseif ($arg1 == "history")
{
    init_database();
    foreach($SQL->query("SELECT hostname,testname,status,timestamp FROM history ORDER BY timestamp DESC") as $row)
    {
        extract($row);
        echo "$timestamp $hostname $testname $status\n";
    }
}
elseif ($arg1 == "showalerts")
{
    init_database();
    foreach($SQL->query("SELECT hostname,testname,recipient,color,timestamp FROM alerts
        ORDER BY timestamp,hostname,testname,recipient") as $row)
    {
        extract($row);
        $ts = date("r",$timestamp);
        echo "$hostname $testname $color $recipient $ts\n";
    }
}
elseif ($arg1 == "clearalerts")
{
    init_database();
    $SQL->exec("DELETE FROM alerts");
    echo "Removed all alerts \n";
}
elseif ($arg1 == "makehistory")
{
    init_database();
    $i=0;
    foreach($SQL->query("SELECT hostname, testname, status, timestamp FROM history ORDER BY timestamp ASC") as $row)
    {
        $i++;
        extract($row);
        list($ts_old,$c_old) = $old_info[$hostname][$testname];
        if ($ts_old)
        {
            $query = "UPDATE history SET timestamp_prev='$ts_old', status_prev='$c_old' WHERE hostname='$hostname' AND testname='$testname' AND timestamp=$timestamp";
            # echo "q: $query\n";
            $SQL->exec($query);
        }
        $old_info[$hostname][$testname] = array ($timestamp,$status);
        if ($i==100) { echo "." ; $i=0;}
    }
    echo " done\n";
}
else
    die ("
Usage:
php setup.php <command> [<arg1>] [<arg2>] ...

Commands:
init                     - create database (if does not exist)
disable <host> <test>    - disable test permanently
isdisabled <host> <test> - check if is disabled
rmtest <host> <test>     - remove all data for the test
rmhost <host>            - remove all data for the host
history                  - show all history for all hosts and tests
hosthist <host>          - show all history for host
testhist <host> <test>   - show all history for host and test
showalerts               - show alerts database
clearalerts              - clear alerts database, so they are sent again
makehistory		 - update history table (for upgrade)
stats                    - statistics (not implemented)
\n");

