<?php
/*
    This script must by run by server periodically.
    Pings all the hosts in 'hosts' using fping.
    Reports host availability directly to omonitor engine.
*/

chdir(dirname(__FILE__)."/..");

if (!file_exists("etc/hosts"))
    die("ERROR Cannot find configuration file etc/hosts\n");

require_once("lib/omonitor.inc.php");

if (!isset($FPING))
    die("ERROR Cannot find fping!\n");

$clients = file_get_contents("etc/hosts");
$clients = preg_replace("/^\s*#.*?$/m","",$clients);
$lines = explode("\n",$clients);
$host_ips = array();
$dialup = array();
foreach($lines as $line)
{
    $lost = 1;
    if (!preg_match("/^(\w[\w.-]+\w)/",$line,$match))
        continue;

    $host = $match[1];
    $ip = $host;

    if (preg_match("/\s(\d+\.\d+\.\d+\.\d+)/",$line,$match))
        $ip = $match[1];

    if (preg_match("/lost=(\d+)/",$line,$match))
        $lost = $match[1];
    if (preg_match("/\sdialup/",$line))
        $dialup[$host] = true;

    $host_ips[$host] = $ip;
    $lostpings[$host] = $lost;

}
$host_names = array_flip($host_ips);

$command = "$FPING -a -c 4 -t 2000 ".join(" ",$host_ips)." 2>&1";
# echo "$command\n";
$handle = popen($command, 'r');
/*
some.domain.com                 : xmt/rcv/%loss = 20/20/0%, min/avg/max = 0.09/0.12/0.15
some.domain.net address not found
*/
$results = array();
while($line = fgets($handle))
{
    //echo "*** got line: $line";
    $color = "green";
    if (preg_match("!^(\S+)\s+address not found!",$line,$match))
    {
        $host = $match[1];
        $color = "red";
        $data    = "address $host not found\n";
        $message = "{red} address $host not found\n";
    }
    else
    {
        //echo "-- $line --\n";
        if (!preg_match("!(\S+)\s+: xmt/rcv/%loss = (\d+)/(\d+)/!",$line,$match))
            continue;
        list($xxx,$ip,$xmt,$rcv) = $match;
        $host = $host_names[$ip];
        //echo "$ip/$host/$xmt/$rcv/".$lostpings[$host]."\n";
        if ($rcv == "0")
        {
            $color = "red";
            $message = "{red} $host is unavailable\n";
        }
        elseif ($rcv <= $xmt - $lostpings[$host])
        {
            $color = "yellow";
            $message = "{yellow} $host is available, some pings lost\n";
        }
        else
        {
            $message = "{green} $host is available\n";
        }
        $data = $line;
    }

    if ($color == "red" && $dialup[$host])
    {
        $color = "clear";
        $message = "dialup down\n";
    }

    $results[$ip] = array($color,$message,$data);
}

$clients = array();
$testname = "conn";
$timestamp = make_timestamp();
init_database();
foreach($host_ips as $host=>$ip)
{
    $color = "red";
    $message = "no data??\n";
    $data = "";
    if (is_array($results[$ip]))
    {
        list($color,$message,$data) = $results[$ip];
    }
    //echo "$host $ip $color ${message}${data}\n";
    put_client_event($host,$testname,$data,$color,$message);
}
close_database();
omon_forward_requests();

