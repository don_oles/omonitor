<?php
/*
    This script must by run by server periodically.
    It performs SNMP checks according to configuration file
    Reports host availability directly to omonitor engine.
*/

chdir(dirname(__FILE__)."/..");

if (!file_exists("etc/hosts"))
    exit;

require_once("lib/omonitor.inc.php");

$checks = file_get_contents("etc/snmp-checks");
$checks = preg_replace("/^\s*#.*?$/m","",$checks);
$checks = preg_replace("/[ \t]+/m"," ",$checks);
$lines = split("\n",$checks);
$datas = array();
$messages = array();
$colors = array();
foreach($lines as $line)
{
    $line = trim($line);
    $tokens = preg_split("/\s+/",trim($line));
    list($host,$community,$snmpvar,$name,$test,$type) = $tokens;
    if (!$host) continue;
    $tokens = array_slice($tokens,6);
    $value = @snmp2_get($host,$community,$snmpvar,5000000,1);
    if ($type == "INTEGER") 
    {
        list($min_r,$min_y,$max_y,$max_r) = $tokens;
        if (value !== false)
        {
            preg_match("/(\d+)/",$value,$match);
            $value = $match[1]+0;
        }
        $c = "green";
        $mess = "";
        if ($value === false)
        {
            $c = "red"; 
            $mess = "can not get data";
        }
        elseif ($value < $min_r)
        {
            $c = "red"; 
            $mess = "$value<$min_r";
        } 
        elseif ($value > $max_r)
        {
            $c = "red"; 
            $mess = "$value>$max_r";
        } 
        elseif ($value < $min_y)
        {
            $c = "yellow"; 
            $mess = "$value<$min_y";
        }
        elseif ($value > $max_y)
        {
            $c = "yellow"; 
            $mess = "$value>$max_y";
        } 
        $data = "$name $value\n";
        if ($mess != "") $mess = "{"."$c} $name $mess\n";
        $datas[$host][$test] .= $data;
        $messages[$host][$test] .= $mess;
        $colors[$host][$test] = compile_color($colors[$host][$test],$c);
    }
    
}
$timestamp = make_timestamp();
init_database();
foreach($colors as $host=>$tests)
{
    foreach($tests as $test=>$color)
    {
        $data = $datas[$host][$test];
        $message = $messages[$host][$test];
        // echo "$host/$test/$color\n$data /// $message\n--------------\n";
        put_client_event($host,$test,$data,$color,$message);
    }
}
close_database();
omon_forward_requests();
