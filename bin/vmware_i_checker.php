<?php
/*
    This script must by run by server periodically.
    Checks all URL's in 'clients', downloading those pages
    and searching for given text/pcre.
    Reports URL availability directly to omonitor engine.
*/

chdir(dirname(__FILE__)."/..");
require_once("lib/vmware.inc.php");
require_once("lib/omonitor.inc.php");

$all_clients = get_clients();
$datastores = array();
foreach($all_clients as $client)
{
    list($host,$tests) = $client;
    if (!preg_match("/^\w[\w.-]+\w$/",$host))
        continue;
    $tests = split("\n",$tests);
    foreach($tests as $test)
    {
        $test = trim($test);
        if (!preg_match("!^VI_DS (.*)$!i",$test,$match))
            continue;
        list($dsname,$yellow,$red) = preg_split("/\s+/",$match[1]);
        if ($dsname != "") $datastores[$dsname] = array($host,$yellow,$red);
    }
}

$datas = $colors = $messages = array();

foreach($VI_SERVERS as $VI_SERVER)
{
    list($VI_SERVER_URL,$VI_USER,$VI_PASSWORD) = $VI_SERVER;
    $vi = new VI_simple($VI_SERVER_URL,$VI_USER,$VI_PASSWORD);
    $vi_datastores = $vi->get_datastores();

    foreach($vi_datastores as $oid=>$props)
    {
	$summary = $props['summary'];
	$dsname = $summary->name;

        $ds_test = $datastores[$dsname];
        if (!is_array($ds_test))
            continue;
        list($host,$ds_yellow,$ds_red) = $ds_test;
        if (!$ds_yellow) $ds_yellow = 90;
        if (!$ds_red) $ds_red = 95;

	$dsspace= bcdiv($summary->capacity,1024,0);
	$dsfree = bcdiv($summary->freeSpace,1024,0);
	$dsurl	= $summary->url;
	$dstype	= $summary->type;

        if ($colors[$host] == "")
            $colors[$host] = "green";
        if ($messages[$host] === null)
            $messages[$host] = "";

        $ds_used = bcdiv(($dsspace - $dsfree) * 100 , $dsspace,1);
        $datas[$host] .= sprintf("%s %16s %12s %12s %4s%% %32s\n", $dstype,$dsname,$dsspace,$dsfree,$ds_used,$dsurl);
        if ($ds_used >= $ds_red)
            $color = "red";
        elseif ($ds_used >= $ds_yellow)
            $color = "yellow";
        else
            $color = "green";
        $colors[$host] = compile_color($colors[$host],$color);
        $messages[$host] .= "{{$color}} $dsname $ds_used%\n";
    }

}

$testname = "vi_ds";

init_database();
foreach($colors as $host=>$color)
{
    $message = $messages[$host];
    $data = $datas[$host];
    put_client_event($host,$testname,$data,$color,$message);
    update_client_history($host,$testname);
}
close_database();
omon_forward_requests();

