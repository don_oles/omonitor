<?php

//$SRVLOG="logs/server-daemon.log";
$PIDFILE="tmp/omonitor.pid";
$SERVER_PORT=1975;
$SERVER_BIND=0;

chdir(dirname(__FILE__)."/..");
require_once("lib/omonitor.inc.php");
require_once("lib/omonitor-socket-server.inc.php");

$options = getopt("dh");
shift_opts($options);
$command = $argv[1];

$NODETACH = ($options["d"]===false);

switch($command)
{
    case "stop":
        stop_daemon();
        exit;
    case "restart":
        stop_daemon();
        break;
    case "start":
        break;
    default:
        echo $argv[0]." [-d] <start|stop|restart>\n";
        exit;
}

init_database();
close_database();

@ob_end_flush();
set_time_limit(0);
declare(ticks = 1);

if (!$NODETACH)
{
    $pid = pcntl_fork();
    if ($pid == -1) // error
        die("Error! Can not fork!\n");
    elseif ($pid) // parent
    {
        echo "Forked. Parent exits.\n";
        exit();
    }
    if (!posix_setsid())
        die_log("Error! Can not become session leader!\n");
}
else
    echo "Running in foreground, logging to console with extra debug output\n";


if (!pcntl_signal(SIGCHLD, "sig_handler"))
    die_log("Can not install signal handler");
if (!pcntl_signal(SIGTERM, "sig_handler"))
    die_log("Can not install signal handler");
if (!pcntl_signal(SIGINT, "sig_handler"))
    die_log("Can not install signal handler");

$pid = posix_getpid();
file_put_contents($PIDFILE,"$pid\n");

init_database();
$daemon = new socketDaemon();
$server = $daemon->create_server('omonServer', 'omonServerClient', $SERVER_BIND, $SERVER_PORT);
write_log("starting accepting connections");
$daemon->process();


//----------------

