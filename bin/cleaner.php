<?php

/*
    This script must by run by server periodically.
    It looks for tests not updated for a $PURPLETIME minutes
    and makes them purple. The 'already purple' statuses are updated
    to reflect for how long they were not updated.
*/

chdir(dirname(__FILE__)."/..");
require_once("lib/omonitor.inc.php");

//sleep(30);

init_database();

$timestamp = make_timestamp();

$currts = gmmktime();

$rows=array();
$rc = $SQL->query("SELECT hostname,testname,timestamp,status FROM current");
while($row = $rc->fetch())
    $rows[]=$row;

foreach($rows as $row)
{
    list($host,$test,$ts2,$color) = $row;

    //echo "$host $test $ts2 $color\n";
    //continue;

    if ($color == "purple")
    {
        // update current purple status
        // look for previous event;
        $rc2 = $SQL->query("SELECT status,timestamp
            FROM history
            WHERE hostname=".sqlsq($host)."
                AND testname=".sqlsq($test)."
            ORDER BY timestamp DESC
            LIMIT 1");
        list($c,$ts3) = $rc2->fetch(); // this purple status
        //list($c,$ts)  = $rc2->fetch();
        $ts = make_unix_timestamp($ts3);
        $diff = $currts - $ts;
        $difftext = time_diff_text($diff);
        $message = "{purple} No update for $difftext, previous status was $color\n";
        put_client_event($host,$test,"*** no data\n","purple",$message,$ts3);
        //echo "put purple client event\n";
        continue;
    }

    $ts = make_unix_timestamp($ts2);
    $diff = $currts - $ts;

    write_debug("difference for $host/$test is $currts - $ts = $diff ($ts2)");

    if ($diff > $PURPLETIME*60)
    {
        $difftext = time_diff_text($diff);
        $message = "{purple} No update for $difftext, previous status was $color\n";
        put_client_event($host,$test,"*** no data\n","purple",$message);
    }
}
close_database();
//omon_forward_requests(); // we do not forward purple messages

