    Look doc directory for instructions on installing and using this nice tool.
    License is there too.
    Oles Hnatkevych, don_oles@able.com.ua

***************************************
* OMONITOR - SIMPLE MONITORING SYSTEM *
***************************************

Influenced by Big Brother. Simple configuration. Most checks performed at server side. Free.
Uses port 1975 by default.

*** WHY OMONITOR?

There are many monitoring systems. Why another one? Ok, let's see what we have by hand.
Known for me were Big Brother, Hobbit, Zabbix, Nagios. Being happy with BB after trying 
Zabbix and Nagios they seems so much complex and heavy for a simple task of monitoring 
several simple things. Nagios is monstrous and pain to configure. Zabbix has problems 
with complexity too. Zabbix also depends on external database.
ONE OF THE MOST IMPORTANT THING FOR ANY PROGRAM IS PLAIN AND EASY CONFIGURATION.
Big Brother is good, howerver it has problems:
* not actually free
* tests performed by clients, so every client has its own configuration copy,
  which is meant to be 'shared' by means of replication
* must be compiled
Hobbit has problems too:
* file layout mess
* too complex for a simple task
* db maintenance ugly (removing host not always works)
* must be compiled
After fighting with both of them, I decided that a simple task should require a simple tool.
Using high-level language (as PHP is) will benefit configuration task.

***  IDEOLOGY of a GOOD SIMPLE MONITOR

Agents (clients) should be lightweight, require basic tools, very easy configurable.
Server has to be lightweight too. PHP is the language of the choice. Nothing to compile.
Server has simple configuration. Use regular expressions when possible, it's a powerful thing.
Tests should be performed server-side, but not necessarily.
Servers accept data from agents, hold database, have web-interface, send alerts.
Servers may forward messages to other servers, creating a tree-like structure.
Web-interface has to be BBTray compatible, and simple as Big Brother or Hobbit have.
Do not care about compatiblity with BB and Hobbit.
Use Windows tools and API for monitoring Windows boxes (COM/WMI).
Do not make graphs, use Munin instead.
KISS (keep it simple).

