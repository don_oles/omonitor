<?php
/*
    This one script is for running one test on one host
    once and send to server and exit.
*/

chdir(dirname(__FILE__));
require_once("omonitor.lib.php");
$CLI = true;

$hostname = $argv[1];
$testname = $argv[2];

if ($hostname == "" || $testname=="")
{
    die("Provide hostname and testname\n");
}

$wmi = create_wmi_object($hostname);
if (!$wmi)
    die();

$dnshostname = (strstr($hostname,".")===false) ? "${hostname}.{$DNSDOMAIN}" : $hostname;

$messages = array();

$timestamp = make_timestamp();


if (preg_match("/^(proc|disk|serv)$/",$testname))
{
    $data = call_user_func("get_test_${testname}",$wmi);
    if ($data == "")
        continue;
    $mess = "[info $dnshostname $testname $timestamp Windows]\n$data";
    $messages[]=$mess;
}
/*
if ($testname == "proc")
{
    $data = get_test_proc($wmi);
    if ($data == "")
        continue;
    $mess = "[info $dnshostname $testname $timestamp Windows]\n$data";
    $messages[]=$mess;
}

if ($testname == "disk")
{
    $data = get_test_disk($wmi);
    if ($data == "")
        continue;
    $mess = "[info $dnshostname $testname $timestamp Windows]\n$data";
    $messages[]=$mess;
}

if ($testname == "serv")
{
    $data = get_test_serv($wmi);
    if ($data == "")
        continue;
    $mess = "[info $dnshostname $testname $timestamp Windows]\n$data";
    $messages[]=$mess;
}
*/

if (preg_match("/^asm$/",$testname))
{
    $command = "$PSTOOLS\\psexec.exe \\\\$dnshostname \"$ASM_RAIDUTIL\" -L raid";
    //$command = "$PSTOOLS\\psexec.exe";
    //echo "command: $command\n";
    $data = exec($command,$data_arr);
    $data = join("\n",$data_arr);
    if ($data == "")
        continue;
    $mess = "[info $dnshostname $testname $timestamp Windows]\n$data";
    $status = "green";
    if (preg_match("/(failed|degraded)/i",$data))
        $status = "red";
    $mess .= "\n[status $status]\n";
    //echo $mess;
    $messages[]=$mess;
}

send_all_messages($messages);
write_log("Finished $hostname/$testname");

