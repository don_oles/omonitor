<?php

// default settings
$SERVICE="Omonitor";
$PORT = "1975";
$LOGFILE="omonitor.log";
$SLEEP = 3;
$DELAY = 300;
$DOMAIN="";
$DNSDOMAIN="";
$PSTOOLS = "C:\pstools";
$ASM_RAIDUTIL="C:\\Program Files\\Adaptec\\Storage Manager\\raidutil.exe";

$DEBUG=false;

require_once("omon.config.php");

if ($SERVER=="")
    die("Please setup \$SERVER in omon.config.php!");

if ($DNSDOMAIN=="" && $DOMAIN!="")
    $DNSDOMAIN = $DOMAIN;

check_for_modules();

//-------------------------------------------------------------------

function write_log($text,$exitcode=null)
{
    global $LOGFILE;
    global $CLI;
    $text = date("r")." $text\n";
    if ($CLI)
        echo $text;
    if ($LOGFILE!="")
        file_put_contents($LOGFILE,$text,FILE_APPEND);
    if ($exitcode !== null)
        exit($exitcode);
}

function fetch_stations_config()
{
    $stations = file_get_contents("stations");
    $stations = preg_replace("/\s*#.*$/m","",$stations);
    $stations = preg_replace("/^\s*/m","",$stations);
    $stations = preg_replace("/\s*$/m","",$stations);
    $stations = preg_replace("/\s{2,}/"," ",$stations);
    $stations = split("\n",$stations);
    return $stations;
}

function process_all_stations()
{
    global $DNSDOMAIN,$DEBUG;
    $stations = fetch_stations_config();
    $messages = array();
    foreach($stations as $st)
    {
        $st = preg_split("/\s+/",$st);
        $hostname = array_shift($st);
        $wmi = create_wmi_object($hostname);

        if (!$wmi)
            continue;

        $dnshostname = (strstr($hostname,".")===false) ? "${hostname}.{$DNSDOMAIN}" : $hostname;
        foreach($st as $testname)
        {
            if ($DEBUG) write_log ("testing $hostname $testname");
            //echo "$hostname $testname ...\n";
            $timestamp = make_timestamp();
            
            if (preg_match("/^(proc|disk|serv)$/",$testname))
            {
                $data = call_user_func("get_test_{$testname}",$wmi);
                if ($data == "")
                    continue;
                $mess = "[info $dnshostname $testname $timestamp Windows]\n$data";
                $messages[]=$mess;
            }
/*
            if ($testname == "proc")
            {
                $data = get_test_proc($wmi);
                if ($data == "")
                    continue;
                $mess = "[info $dnshostname $testname $timestamp Windows]\n$data";
                $messages[]=$mess;
            }
            if ($testname == "disk")
            {
                $data = get_test_disk($wmi);
                if ($data == "")
                    continue;
                $mess = "[info $dnshostname $testname $timestamp Windows]\n$data";
                $messages[]=$mess;
            }
            if ($testname == "serv")
            {
                $data = get_test_serv($wmi);
                if ($data == "")
                    continue;
                $mess = "[info $dnshostname $testname $timestamp Windows]\n$data";
                $messages[]=$mess;
            }
*/

            // psexec.exe \\portal.synapse.com "C:\Program Files\Adaptec\Storage Manager\raidutil.exe" -L raid
            if (preg_match("/^asm$/",$testname))
            {


                global $PSTOOLS,$ASM_RAIDUTIL;

                $command = "$PSTOOLS\\psexec.exe \\\\$dnshostname \"$ASM_RAIDUTIL\" -L raid";
                //echo "command: $command\n";
                $data = exec($command,$data_arr);
                $data = join("\n",$data_arr);
                if ($data == "")
                    continue;
                $status = "green";
                if (preg_match("/(failed|degraded)/i",$data))
                    $status = "red";
                if (preg_match("/(reconstruct|replaced)/i",$data))
                    $status = "yellow";
                $mess = "[info $dnshostname $testname $timestamp Windows]\n$data\n[status $status]\n";
                //echo $mess;
                $messages[]=$mess;
            }
        }
        unset($wmi);
    }
    send_all_messages($messages);
}

function send_all_messages($messages)
{
    global $SERVER,$PORT,$CLI,$DEBUG;

    $message = join("",$messages)."[-- end --]\n";
    if ($DEBUG && $CLI) { echo $message; }

    $fh = @fsockopen($SERVER,$PORT,$errno,$errstr);
    if (!$fh)
    {
        write_log("Can not connect to server $SERVER:$PORT : $errno $errstr");
        return;
    }
    fwrite($fh,$message);
    fclose($fh);
}

function create_wmi_object($hostname)
{
    global $DOMAIN;
    global $CLI, $DEBUG;
    if (strstr($hostname,".")===false && $DOMAIN!="")
        $hostname .= ".{$DOMAIN}";

    $dsn = "winmgmts://$hostname/root/CIMV2";
    if ($DEBUG) write_log ("dsn: $dsn");


    try
    {
        $obj = new COM ( $dsn );
    }
    catch (com_exception $e)
    {
        $obj = null;
        write_log("Can not connect to $hostname: ".$e->getMessage());
    }

    return $obj;
}

function make_timestamp($ts=0)
{
    if ($ts)
        return gmdate("YmdHis",$ts);
    return gmdate("YmdHis");
}


function get_test_serv($obj)
{
    $format = "%-32s %-8s %-7s\n";
    // $res = sprintf($format,"Service", "Mode", "State"); 
    $servs = $obj->ExecQuery("Select * from Win32_Service");
    foreach($servs as $serv)
    {
        $caption = $serv->Caption;
        $dn = $serv->DisplayName;
        $name = str_replace(" ","",$serv->Name);
        $started = $serv->Started;
        $startmode = $serv->StartMode;
        $state = $serv->State;
        $res .= sprintf($format,$name, $startmode, $state);
    }
    unset($servs);
    return $res;
}


/*
omon@confluence-kbp ~/omonitor/bin $ df
Filesystem           1K-blocks      Used Available Use% Mounted on
/dev/sda6            137011088  41678344  88372988  33% /
udev                   2027188       612   2026576   1% /dev
shm                    2027188         0   2027188   0% /dev/shm
*/
function get_test_disk($obj)
{
    $res = "Filesystem   Size        Used        Free Use LogicalDisk\n";
    $disks = $obj->ExecQuery("Select * from Win32_LogicalDisk WHERE DriveType=3"); // local drives
    foreach($disks as $disk)
    {
        // $size = intval($disk->Size / 1024);
        $size = bcdiv($disk->Size,1024,0);
        $fs = $disk->FileSystem;
        // $free = intval($disk->FreeSpace / 1024);
        $free = bcdiv($disk->FreeSpace, 1024);
        $mp = $disk->DeviceID;
        // $used = $size - $free;
        $used = bcsub($size,$free);
        $usage = 100 * bcdiv($used, $size, 2);
        $res .= sprintf("%5s %11s %11s %11s %2d%% %s\n",$fs,$size,$used,$free,$usage,$mp);
    }
    unset($disks);
    return $res;
}

/*
  PID  PPID USER      STARTED S PRI %CPU     TIME %MEM   RSZ    VSZ CMD
18091 12262 root       Jan 14 S  23  0.0 00:00:00  0.0  1460  44112 /usr/sbin/winbindd
*/
function get_test_proc($obj)
{
    $res = " PID PPID PRIO      WSS         VS NAME\n";
    $processes = $obj->ExecQuery("Select * from Win32_Process");
    foreach($processes as $proc)
    {
        $pid = $proc->ProcessID;
        $ppid = $proc->ParentProcessID;
        $prio = $proc->Priority;
        $name = $proc->CommandLine;
        if ($name=="")
            $name = "- ".$proc->Name;
        $wss = intval($proc->WorkingSetSize/1024);
        $vs = intval($proc->VirtualSize/1024);
        $res .= sprintf("%4d %4d %2d %10d %10d %s\n",$pid,$ppid,$prio,$wss,$vs,$name); //" $pid\t$ppid\t$prio\t$wss\t$vs\t$name\n";
    }
    unset($processes);
    return $res;
}

function check_for_modules()
{
    $missing = 0;
    $modules = array(
        "pcre"          => "preg_match",
        "date"          => "gmmktime",
        "sockets"       => "socket_create",
        "win32service"  => "win32_delete_service",
    );
    foreach($modules as $mod=>$func)
        if (!function_exists($func))
        {
            $missing++;
            echo "module missing: $mod\n";
        }
    if (!class_exists('COM'))
    {
        $missing++;
        echo "module missing: COM\n";
    }
    if ($missing)
        die ("Please install all the necessary modules!\n");
}

