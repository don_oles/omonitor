<?php
/*
    This one script is for running all tests on all hosts
    once and send to server and exit.
*/

chdir(dirname(__FILE__));
require_once("omonitor.lib.php");
$CLI = true;
$DEBUG = true;

process_all_stations();

write_log("Finished process_all_stations");
