<?php
/*
    This is a service that you have to 'register'.
    It will wake up regularly and do the job of processing all client tests.

    Usage: php __FILE__ <register|unregister>
*/


chdir(dirname(__FILE__));

require_once("omonitor.lib.php");

$DISPLAY="Omonitor agent $SERVER:$PORT";

$action = $argv[1];

$CLI = true;

if ($action == "register")
{
    $res = win32_create_service(array(
        "service"       => $SERVICE,
        "display"       => $DISPLAY,
        "params"        => __FILE__ ." run",
    ));
    if ($res === true)
    {
        write_log("Service $SERVICE registered");
        echo "Now configure the service to use account that can collect WMI data from remote hosts!\n";
        exit;
    }
    else
        write_log("Error registering service $SERVICE: $res",1);
}

if ($action == "unregister")
{
    $res = win32_delete_service($SERVICE);
    if ($res === true || $res == 0)
        write_log("Service $SERVICE unregistered",0);
    else
        write_log("Error unregistering service $SERVICE: $res",2);
}

if ($action != "run")
{
    echo "
Usage: php ".__FILE__." <register|unregister>

";
    write_log("Run without proper arguments",3);
}

$res = win32_start_service_ctrl_dispatcher($SERVICE);
if ($res !== true)
{
    write_log("I'm probably not running under the service control manager; err: $res", 4);
}
$res = win32_set_service_status(WIN32_SERVICE_RUNNING);

$CLI = false;

write_log("Service started");

$paused = false;
$counter = $DELAY ; // run
while(true)
{
    $mess = win32_get_last_control_message();
    if ($mess == WIN32_SERVICE_CONTROL_STOP)
    {
        $res = win32_set_service_status(WIN32_SERVICE_STOPPED);
        write_log("Got stop message, stopping: $res");
        break;
    }

    if ($mess == WIN32_SERVICE_CONTROL_PAUSE)
    {
        $res = win32_set_service_status(WIN32_SERVICE_PAUSED);
        write_log("Got pause message, pausing: $res");
        write_log("Resetting counters for pause");
        $paused = true;
        $counter = 0; // for any case
        sleep ($SLEEP);
        continue;
    }
    if ($mess == WIN32_SERVICE_CONTROL_INTERROGATE)
    {
        $res = win32_set_service_status(WIN32_SERVICE_RUNNING);
        if ($DEBUG) write_log("Got interrogate message, answered: $res");
    }
    if ($mess == 0)
    {
        //$res = win32_set_service_status(WIN32_SERVICE_RUNNING);
        //if ($DEBUG) write_log("Got NUL message, answered: $res");
    }

    if ($mess == WIN32_SERVICE_CONTROL_CONTINUE)
    {
        $res = win32_set_service_status(WIN32_SERVICE_RUNNING);
        if ($paused)
        {
            write_log("Got continue message, awakening: $res");
            $paused = false;
            $counter = $DELAY;
        }
    }

    if (!$paused)
    {
        if ($DEBUG) write_log("$counter $DELAY $SLEEP");
        if ($counter >= $DELAY)
        {
            process_all_stations();
            if ($DEBUG) write_log("data collected and sent");
            $counter = 0;
        }
        $counter += $SLEEP;
    }
    sleep ($SLEEP);
}

win32_set_service_status(WIN32_SERVICE_STOPPED);
write_log("Service stopped");

